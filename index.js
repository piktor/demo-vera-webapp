const rec = require("./server/recommendation");
const express = require("express");
const mongoose = require("mongoose");
const app = express();
const cors = require("cors");
const path = require("path");
const https = require("https");
const axios = require("axios");
const bodyParser = require("body-parser");

const Product = require("./server/models/product");
const config = require("./server/config/config");

const port = process.env.port || config.port;

const instance = axios.create({
  httpsAgent: new https.Agent({
    rejectUnauthorized: false
  })
});

mongoConnect = async () => {
  await mongoose
    .connect(config.mongodb, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    })
    .then(() => console.log("connected"))
    .catch(e => console.error(e));
};

mongoConnect();

app.use(
  cors({
    origin: "*"
  })
);

app.use(express.static(path.join(__dirname, "/front-end/build")));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.route("/products").get((req, res) => {
  const type = req.query.type || null;
  const gender = req.query.gender || null;
  const skip = (parseInt(req.query.page) + 1) * 24 || 0;
  if (config.productType.includes(type)) {
    let countPromise = Product.find({}).count();
    let obj = {};
    if (gender) obj.gender = gender;
    if (type) obj.type = type;

    let sendPromise = Product.find(obj)
      .limit(24)
      .skip(skip);

    let promiseArr = Promise.all([sendPromise, countPromise]);

    promiseArr
      .then(products => {
        res.send({
          products: products[0],
          length: products[0].length,
          totalCount: products[1]
        });
      })
      .catch(e => {
        res.send({ result: "Not Found", error: true });
      });
  } else {
    let obj = {};
    let promise = Product.aggregate([{ $sample: { size: 24 } }]);
    const gender = req.query.gender || null;
    if (gender) {
      obj = { $match: { gender: gender } };
      (promise = Product.find({ gender }))
        .limit(24)
        .skip(Math.floor(Math.random * 24));
    }

    promise
      .then(products => {
        res.send({ products, count: products.length });
      })
      .catch(e => {
        res.send({ results: { products: "Not Found", error: true } });
      });
  }
});

app.route("/product").get((req, res) => {
  const id = req.query.id;
  console.log(id);
  Product.findById(id)
    .then(result => {
      res.send({ result, error: false });
    })
    .catch(e => {
      res.send({ result: "Not Found", error: true });
    });
});

app.route("/recommendation").post((req, res) => {
  let body = req.body;
  let vendor = body.vendor || "Walmart";
  let id = body.id || null;
  let image_url = body.image_url;
  // let url =
  //   "https://35.222.17.36/v3/recommendations?source=SAMPLE&algorithm=annoy&nitems=10&&social=VERA&category=top" +
  //   "&vendor=" +
  //   vendor;
  let url =
    "https://35.222.17.36/v3/recommendations?source=Walmart&algorithm=annoy&nitems=11&vendor=Target&social=vera_funnel&category=top";

  let obj = {
    image_url
  };

  // if (id) obj.id = id
  const headers = {
    "Content-Type": "application/json"
  };
  instance
    .post(url, obj, {
      headers: headers
    })
    .then(result => {
      res.send({ data: result.data, error: false });
    })
    .catch(e => {
      res.send({ error: true });
    });
});
// https://35.222.17.36/

app.route("/imagesearch").post((req, res) => {
  let { data, params } = req.body;
  let { id, image_url } = data;
  data = {
    id,
    image_url
  };
  let url =
    "https://35.222.17.36/v3/imagesearch/?algorithm=annoy&nitems=9&ouput_type=raw_result&verbose=True";

  instance
    .post(url, data, { params })
    .then(result => {
      console.log(JSON.stringify(result.data.similar_data));
      let data = result.data.similar_data.top
        ? result.data.similar_data.top["0"].similar_items.data
        : [];
      console.log(data);
      res.send({ data, error: false });
    })
    .catch(e => {
      res.send({ error: true, data: [] });
    });
});

app.route("/wallmart").get((req, res) => {
  const tcin = req.query.id;
  const val = rec.top_to_wallmart(tcin);
  if (val.error) {
    res.send({ result: "Not Found", error: true });
  } else {
    Product.find({
      tcin: { $in: val }
    })
      .then(result => {
        let product = null;
        result.map(e => {
          if (e.tcin === tcin) product = e;
        });
        let productArr = [];
        result.map(e => {
          if (e.tcin !== tcin) productArr.push(e);
        });

        res.send({ product, result: productArr, length: result.length });
      })
      .catch(e => {
        res.send({ result: "Not Found", error: true });
      });
  }
});

if (module === require.main) {
  const server = app.listen(port, () => {
    const port = server.address().port;
    console.log(`App listening on port ${port}`);
  });
}

module.exports = app;

// app.listen(port, () => {
//   console.log("Listening at port 5000");
// });
