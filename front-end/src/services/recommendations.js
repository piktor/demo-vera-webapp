export const recommendation = {
  input_data: {
    id: "52934563",
    title:
      "Women&#39;s Plus Size Star Wars: The Last Jedi Criss Cross V-Neck Short Sleeve Graphic T-Shirt (Juniors&#39;) - Black 2X",
    category: "top",
    subcategory: "Tee shirts",
    upc: "490060605367",
    image_url:
      "https://target.scene7.com/is/image/Target/GUEST_9914d966-60fe-401a-a60c-8a3a6da16727"
  },
  social_similar_data: {
    top: {
      similar_data: {
        top: {
          "0": {
            similar_items: {
              data: [
                {
                  Top: {
                    box: [141, 2, 314, 306],
                    style: "blouse",
                    neck: "mock neck",
                    sleeve: "long",
                    pattern: "solids",
                    length: "mid",
                    colors: {
                      LAB: [
                        {
                          $numberDouble: "7.254110064665188"
                        },
                        {
                          $numberDouble: "0.07836059029096842"
                        },
                        {
                          $numberDouble: "1.3835118866613705"
                        }
                      ]
                    },
                    detail: "plain",
                    material: "satin"
                  },
                  name: "702209766870800893.jpg",
                  Bottom: {
                    box: [161, 154, 301, 345],
                    colors: {
                      LAB: [
                        {
                          $numberDouble: "69.435714749908"
                        },
                        {
                          $numberDouble: "-1.2574699880897877"
                        },
                        {
                          $numberDouble: "-0.7932941406785687"
                        }
                      ]
                    },
                    type: "Skirt",
                    pattern: "solids",
                    length: "short",
                    detail: "ruffle",
                    skirt_type: "straight"
                  },
                  img_url:
                    "https://i.pinimg.com/736x/b4/79/5c/b4795c4a53fcc70087557bb9ac057d13.jpg",
                  verified_tags: {
                    verified_tags: [
                      {
                        top: {
                          style: "blouse",
                          neck: "mock neck",
                          sleeve: "long",
                          pattern: "solids",
                          material: "wool",
                          detail: "plain",
                          length: "mid",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "7.284905223003268"
                                },
                                {
                                  $numberDouble: "-0.000523835567950437"
                                },
                                {
                                  $numberDouble: "1.4451791028233796"
                                }
                              ]
                            }
                          },
                          hexNum: "171614"
                        },
                        bottom: {
                          type: "Skirt",
                          pattern: "solids",
                          length: "mini",
                          detail: "pleat",
                          skirt_type: "straight",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "69.3244966681508"
                                },
                                {
                                  $numberDouble: "-1.21709295019784"
                                },
                                {
                                  $numberDouble: "-1.015832156638119"
                                }
                              ]
                            }
                          },
                          hexNum: "A6AAAB"
                        },
                        reject: true
                      },
                      {
                        top: {
                          style: "blouse",
                          neck: "mock neck",
                          sleeve: "long",
                          length: "mid",
                          material: "wool",
                          detail: "plain",
                          pattern: "solids",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "7.284905223003268"
                                },
                                {
                                  $numberDouble: "-0.000523835567950437"
                                },
                                {
                                  $numberDouble: "1.4451791028233796"
                                }
                              ]
                            }
                          },
                          hexNum: "171614"
                        },
                        bottom: {
                          type: "Skirt",
                          length: "short",
                          skirt_type: "straight",
                          detail: "ruffle",
                          pattern: "solids",
                          material: "cotton",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "69.3244966681508"
                                },
                                {
                                  $numberDouble: "-1.21709295019784"
                                },
                                {
                                  $numberDouble: "-1.015832156638119"
                                }
                              ]
                            }
                          },
                          hexNum: "A6AAAB"
                        },
                        inappropriate: false,
                        swimwear: false,
                        sleepwear: false,
                        Celebrity: false,
                        logo: false,
                        notwoman: false,
                        reject: false
                      },
                      {
                        top: {
                          style: "blouse",
                          neck: "mock neck",
                          sleeve: "long",
                          length: "mid",
                          pattern: "solids",
                          detail: "plain",
                          material: "satin",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "7.284905223003268"
                                },
                                {
                                  $numberDouble: "-0.000523835567950437"
                                },
                                {
                                  $numberDouble: "1.4451791028233796"
                                }
                              ]
                            }
                          },
                          hexNum: "171614"
                        },
                        bottom: {
                          type: "Skirt",
                          length: "short",
                          skirt_type: "straight",
                          detail: "ruffle",
                          pattern: "solids",
                          material: "chiffon",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "69.3244966681508"
                                },
                                {
                                  $numberDouble: "-1.21709295019784"
                                },
                                {
                                  $numberDouble: "-1.015832156638119"
                                }
                              ]
                            }
                          },
                          hexNum: "A6AAAB"
                        },
                        inappropriate: false,
                        swimwear: false,
                        sleepwear: false,
                        Celebrity: false,
                        logo: false,
                        notwoman: false,
                        reject: true
                      }
                    ],
                    verified_by: [
                      "5b20795400cbc699af0df1ad",
                      "5b223f982756dd78715370c8",
                      "5b223ff82756dd78715370c9"
                    ]
                  },
                  hold: true,
                  reviewed_tags: {
                    top: {
                      style: "",
                      neck: "",
                      sleeve: "",
                      length: "",
                      pattern: "",
                      detail: "",
                      material: ""
                    },
                    bottom: {
                      ankle: "",
                      fit: "",
                      type: "",
                      length: "",
                      pattern: "",
                      detail: "",
                      material: ""
                    },
                    reviewed_by: ""
                  },
                  classification: {
                    classified: {
                      top: 1,
                      bottom: 0
                    },
                    on_classification: {
                      top: 0,
                      bottom: 1
                    },
                    classified_as: {
                      top: "tee",
                      bottom: ""
                    }
                  },
                  common: {
                    color: {
                      top: {
                        done: 0,
                        on_process: 1,
                        value: ""
                      },
                      bottom: {
                        done: 0,
                        on_process: 0,
                        value: ""
                      }
                    }
                  },
                  validity: {
                    validated: 1,
                    on_validation: 0,
                    invalid: 0,
                    reason: "valid"
                  },
                  id: "5acdd525be26511bee8a82f1",
                  lastModified: "2019-11-29T19:32:36.722000",
                  processing: {
                    meta: {
                      flag: true
                    }
                  },
                  __order: 0
                },
                {
                  Top: {
                    box: [223, 197, 489, 502],
                    style: "blouse",
                    neck: "mock neck",
                    sleeve: "three-quater",
                    pattern: "solids",
                    material: "wool",
                    detail: "plain",
                    length: "crop",
                    colors: {
                      LAB: [
                        {
                          $numberDouble: "6.572356718665322"
                        },
                        {
                          $numberDouble: "1.7387950251322448"
                        },
                        {
                          $numberDouble: "1.333293869966684"
                        }
                      ]
                    }
                  },
                  name: "187110559501994407.jpg",
                  Bottom: {
                    box: [147, 413, 676, 967],
                    colors: {
                      LAB: [
                        {
                          $numberDouble: "36.417634489379886"
                        },
                        {
                          $numberDouble: "-12.585035335706845"
                        },
                        {
                          $numberDouble: "9.821113585344122"
                        }
                      ]
                    },
                    type: "Skirt",
                    pattern: "solids",
                    length: "maxi",
                    material: "velvet",
                    detail: "pleat",
                    skirt_type: "flared"
                  },
                  img_url:
                    "https://i.pinimg.com/736x/d5/3f/14/d53f147d841f3dce72c171e2da8d8e49.jpg",
                  verified_tags: {
                    verified_tags: [
                      {
                        top: {
                          style: "blouse",
                          neck: "round neck",
                          sleeve: "three-quater",
                          length: "mid",
                          pattern: "solids",
                          detail: "plain",
                          material: "satin",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "6.698082890559213"
                                },
                                {
                                  $numberDouble: "1.622008737334757"
                                },
                                {
                                  $numberDouble: "1.2501196434211825"
                                }
                              ]
                            }
                          },
                          hexNum: "181413"
                        },
                        bottom: {
                          type: "Skirt",
                          length: "maxi",
                          skirt_type: "flared",
                          detail: "pleat",
                          material: "velvet",
                          pattern: "solids",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "36.41669906428054"
                                },
                                {
                                  $numberDouble: "-12.69857319972481"
                                },
                                {
                                  $numberDouble: "9.547345958676711"
                                }
                              ]
                            }
                          },
                          hexNum: "465B46"
                        },
                        inappropriate: false,
                        swimwear: false,
                        sleepwear: false,
                        Celebrity: false,
                        logo: false,
                        notwoman: false,
                        reject: true
                      },
                      {
                        top: {
                          style: "tee",
                          neck: "mock neck",
                          sleeve: "long",
                          length: "mid",
                          pattern: "solids",
                          detail: "plain",
                          material: "not sure",
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "6.572356718665322"
                              },
                              {
                                $numberDouble: "1.7387950251322448"
                              },
                              {
                                $numberDouble: "1.333293869966684"
                              }
                            ]
                          },
                          hexNum: "181413"
                        },
                        bottom: {
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "36.417634489379886"
                              },
                              {
                                $numberDouble: "-12.585035335706845"
                              },
                              {
                                $numberDouble: "9.821113585344122"
                              }
                            ]
                          },
                          type: "Skirt",
                          length: "maxi",
                          skirt_type: "flared",
                          detail: "pleat",
                          pattern: "solids",
                          material: "not sure",
                          hexNum: "465B46"
                        },
                        inappropriate: false,
                        swimwear: false,
                        sleepwear: false,
                        Celebrity: null,
                        logo: false,
                        notwoman: false,
                        reject: true
                      },
                      {
                        top: {
                          style: "tee",
                          neck: "mock neck",
                          sleeve: "three-quater",
                          length: "mid",
                          pattern: "solids",
                          detail: "plain",
                          material: "not sure",
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "6.572356718665322"
                              },
                              {
                                $numberDouble: "1.7387950251322448"
                              },
                              {
                                $numberDouble: "1.333293869966684"
                              }
                            ]
                          },
                          hexNum: "181413"
                        },
                        bottom: {
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "36.417634489379886"
                              },
                              {
                                $numberDouble: "-12.585035335706845"
                              },
                              {
                                $numberDouble: "9.821113585344122"
                              }
                            ]
                          },
                          type: "Skirt",
                          length: "three quarter",
                          skirt_type: "flared",
                          detail: "pleat",
                          pattern: "solids",
                          material: "not sure",
                          hexNum: "465B46"
                        },
                        inappropriate: false,
                        swimwear: false,
                        sleepwear: false,
                        Celebrity: null,
                        logo: false,
                        notwoman: false,
                        reject: true
                      },
                      {
                        top: {
                          style: "tee",
                          neck: "mock neck",
                          sleeve: "three-quater",
                          length: "mid",
                          pattern: "solids",
                          detail: "plain",
                          material: "not sure",
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "6.572356718665322"
                              },
                              {
                                $numberDouble: "1.7387950251322448"
                              },
                              {
                                $numberDouble: "1.333293869966684"
                              }
                            ]
                          },
                          hexNum: "181413"
                        },
                        bottom: {
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "36.417634489379886"
                              },
                              {
                                $numberDouble: "-12.585035335706845"
                              },
                              {
                                $numberDouble: "9.821113585344122"
                              }
                            ]
                          },
                          type: "Skirt",
                          length: "three quarter",
                          skirt_type: "flared",
                          detail: "pleat",
                          pattern: "solids",
                          material: "not sure",
                          hexNum: "465B46"
                        },
                        inappropriate: false,
                        swimwear: false,
                        sleepwear: false,
                        Celebrity: null,
                        logo: false,
                        notwoman: false,
                        reject: true
                      },
                      {
                        top: {
                          style: "blouse",
                          neck: "mock neck",
                          sleeve: "three-quater",
                          length: "crop",
                          pattern: "solids",
                          detail: "plain",
                          material: "wool",
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "6.572356718665322"
                              },
                              {
                                $numberDouble: "1.7387950251322448"
                              },
                              {
                                $numberDouble: "1.333293869966684"
                              }
                            ]
                          },
                          hexNum: "181413"
                        },
                        bottom: {
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "36.417634489379886"
                              },
                              {
                                $numberDouble: "-12.585035335706845"
                              },
                              {
                                $numberDouble: "9.821113585344122"
                              }
                            ]
                          },
                          type: "Skirt",
                          length: "maxi",
                          skirt_type: "flared",
                          detail: "pleat",
                          pattern: "solids",
                          material: "velvet",
                          hexNum: "465B46"
                        },
                        inappropriate: false,
                        swimwear: false,
                        sleepwear: false,
                        Celebrity: null,
                        logo: false,
                        notwoman: false,
                        reject: true
                      },
                      {
                        top: {
                          style: "tee",
                          neck: "mock neck",
                          sleeve: "three-quater",
                          length: "mid",
                          pattern: "solids",
                          detail: "plain",
                          material: "not sure",
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "6.572356718665322"
                              },
                              {
                                $numberDouble: "1.7387950251322448"
                              },
                              {
                                $numberDouble: "1.333293869966684"
                              }
                            ]
                          }
                        },
                        bottom: {
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "36.417634489379886"
                              },
                              {
                                $numberDouble: "-12.585035335706845"
                              },
                              {
                                $numberDouble: "9.821113585344122"
                              }
                            ]
                          },
                          type: "Skirt",
                          length: "maxi",
                          skirt_type: "flared",
                          detail: "pleat",
                          pattern: "solids",
                          material: "not sure"
                        },
                        inappropriate: false,
                        swimwear: false,
                        sleepwear: false,
                        Celebrity: null,
                        logo: false,
                        notwoman: false
                      },
                      {
                        top: {
                          style: "tee",
                          neck: "mock neck",
                          sleeve: "three-quater",
                          length: "mid",
                          pattern: "solids",
                          detail: "plain",
                          material: "not sure",
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "8.342755921176412"
                              },
                              {
                                $numberDouble: "0.9233007615628008"
                              },
                              {
                                $numberDouble: "-3.6618105286222002"
                              }
                            ]
                          }
                        },
                        bottom: {
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "39.57373570128033"
                              },
                              {
                                $numberDouble: "-13.50135529738733"
                              },
                              {
                                $numberDouble: "9.73803413472213"
                              }
                            ]
                          },
                          type: "Skirt",
                          length: "knee",
                          skirt_type: "flared",
                          detail: "pleat",
                          pattern: "solids",
                          material: "not sure"
                        },
                        inappropriate: false,
                        swimwear: false,
                        sleepwear: false,
                        Celebrity: null,
                        logo: false,
                        notwoman: false
                      }
                    ],
                    verified_by: [
                      "5b20795400cbc699af0df1ad",
                      "5b56ec3800cbc601f5f72c94",
                      "5b56ec1700cbc601f5f72c93",
                      "5b2240272756dd78715370ca",
                      "5b223ff82756dd78715370c9",
                      "5b56ebdb00cbc601f5f72c91",
                      "5b2240112756dd786c4d9691"
                    ]
                  },
                  reviewed_tags: {
                    top: {
                      style: "",
                      neck: "",
                      sleeve: "",
                      length: "",
                      pattern: "",
                      detail: "",
                      material: ""
                    },
                    bottom: {
                      ankle: "",
                      fit: "",
                      type: "",
                      length: "",
                      pattern: "",
                      detail: "",
                      material: ""
                    },
                    reviewed_by: ""
                  },
                  classification: {
                    classified: {
                      top: 0,
                      bottom: 0
                    },
                    on_classification: {
                      top: 1,
                      bottom: 1
                    },
                    classified_as: {
                      top: "",
                      bottom: ""
                    }
                  },
                  common: {
                    color: {
                      top: {
                        done: 0,
                        on_process: 0,
                        value: ""
                      },
                      bottom: {
                        done: 0,
                        on_process: 0,
                        value: ""
                      }
                    }
                  },
                  validity: {
                    validated: 1,
                    on_validation: 0,
                    invalid: 0,
                    reason: "valid"
                  },
                  id: "5acdd526be26511bee8a82f6",
                  lastModified: "2019-11-29T19:32:36.722000",
                  processing: {
                    meta: {
                      flag: true
                    }
                  },
                  __order: 1
                },
                {
                  Top: {
                    box: [145, 212, 424, 695],
                    neck: "scoop neck",
                    sleeve: "sleeveless",
                    pattern: "printed",
                    material: "cotton",
                    length: "tunic",
                    style: "tank",
                    pattern_coordinates: [
                      {
                        $numberDouble: "0.3407183587551117"
                      },
                      0,
                      {
                        $numberDouble: "0.4522060751914978"
                      },
                      0,
                      {
                        $numberDouble: "0.9449201822280884"
                      },
                      {
                        $numberDouble: "0.12736904621124268"
                      },
                      0,
                      {
                        $numberDouble: "0.166021466255188"
                      },
                      0,
                      0,
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.49352002143859863"
                      },
                      0,
                      {
                        $numberDouble: "0.2926349937915802"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.532446563243866"
                      },
                      0,
                      {
                        $numberDouble: "0.29087963700294495"
                      },
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.14026018977165222"
                      },
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.21978722512722015"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.01809992641210556"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.3836250603199005"
                      },
                      {
                        $numberDouble: "0.27143096923828125"
                      },
                      {
                        $numberDouble: "0.6630906462669373"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.20328834652900696"
                      },
                      0,
                      {
                        $numberDouble: "0.11764932423830032"
                      },
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.849104106426239"
                      },
                      0,
                      {
                        $numberDouble: "0.9028092622756958"
                      },
                      0,
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.9849730134010315"
                      },
                      {
                        $numberDouble: "0.6522912979125977"
                      },
                      {
                        $numberDouble: "0.24688582122325897"
                      },
                      {
                        $numberDouble: "0.0688202828168869"
                      },
                      0,
                      {
                        $numberDouble: "0.5588655471801758"
                      },
                      0,
                      {
                        $numberDouble: "0.5189802050590515"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.7385504841804504"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.1547948569059372"
                      },
                      {
                        $numberDouble: "0.206420436501503"
                      },
                      {
                        $numberDouble: "0.351926326751709"
                      },
                      0,
                      {
                        $numberDouble: "0.7944084405899048"
                      },
                      {
                        $numberDouble: "0.0020245052874088287"
                      },
                      {
                        $numberDouble: "0.42995837330818176"
                      },
                      {
                        $numberDouble: "0.1015169620513916"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.3512006103992462"
                      },
                      0,
                      {
                        $numberDouble: "0.3680158853530884"
                      },
                      0,
                      {
                        $numberDouble: "0.19076067209243774"
                      },
                      0,
                      {
                        $numberDouble: "0.0758783370256424"
                      },
                      {
                        $numberDouble: "0.015099432319402695"
                      },
                      {
                        $numberDouble: "0.09761451184749603"
                      },
                      {
                        $numberDouble: "0.11510075628757477"
                      },
                      0,
                      {
                        $numberDouble: "0.4483901560306549"
                      },
                      0,
                      {
                        $numberDouble: "0.2361738383769989"
                      },
                      {
                        $numberDouble: "0.7493842244148254"
                      },
                      {
                        $numberDouble: "0.12963968515396118"
                      },
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.7727126479148865"
                      },
                      {
                        $numberDouble: "0.8009496331214905"
                      },
                      0,
                      {
                        $numberDouble: "0.7617506384849548"
                      },
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.5164458751678467"
                      },
                      {
                        $numberDouble: "0.29992204904556274"
                      },
                      {
                        $numberDouble: "0.23325777053833008"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.29579731822013855"
                      },
                      {
                        $numberDouble: "0.3945009112358093"
                      },
                      {
                        $numberDouble: "0.19178506731987"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.3254958987236023"
                      },
                      0,
                      {
                        $numberDouble: "0.62179034948349"
                      },
                      0,
                      {
                        $numberDouble: "0.12621113657951355"
                      },
                      {
                        $numberDouble: "0.23604026436805725"
                      },
                      {
                        $numberDouble: "0.25147226452827454"
                      },
                      0,
                      {
                        $numberDouble: "0.3075365424156189"
                      },
                      {
                        $numberDouble: "0.09745742380619049"
                      },
                      0
                    ],
                    colors: {
                      LAB: [
                        {
                          $numberDouble: "76.19076370845815"
                        },
                        {
                          $numberDouble: "14.616724343710864"
                        },
                        {
                          $numberDouble: "-14.98160482941573"
                        }
                      ]
                    },
                    detail: "lace"
                  },
                  name: "160440805447677820.jpg",
                  Bottom: {
                    box: [163, 669, 415, 799],
                    type: "Pants",
                    fit: "skinny",
                    pattern: "solids",
                    material: "satin",
                    detail: "embroidered",
                    length: "crop",
                    colors: {
                      LAB: [
                        {
                          $numberDouble: "51.38651147950365"
                        },
                        {
                          $numberDouble: "-0.8125624901974549"
                        },
                        {
                          $numberDouble: "-1.914153518666506"
                        }
                      ]
                    }
                  },
                  img_url:
                    "https://i.pinimg.com/736x/7b/2d/b3/7b2db37b2aaeacc9fc263aaa07cdb91c--hippie-fashion-fashion-news.jpg",
                  verified_tags: {
                    verified_tags: [],
                    verified_by: []
                  },
                  reviewed_tags: {
                    top: {
                      style: "",
                      neck: "",
                      sleeve: "",
                      length: "",
                      pattern: "",
                      detail: "",
                      material: ""
                    },
                    bottom: {
                      ankle: "",
                      fit: "",
                      type: "",
                      length: "",
                      pattern: "",
                      detail: "",
                      material: ""
                    },
                    reviewed_by: ""
                  },
                  classification: {
                    classified: {
                      top: 0,
                      bottom: 0
                    },
                    on_classification: {
                      top: 1,
                      bottom: 1
                    },
                    classified_as: {
                      top: "",
                      bottom: ""
                    }
                  },
                  common: {
                    color: {
                      top: {
                        done: 0,
                        on_process: 0,
                        value: ""
                      },
                      bottom: {
                        done: 0,
                        on_process: 0,
                        value: ""
                      }
                    }
                  },
                  validity: {
                    validated: 1,
                    on_validation: 0,
                    invalid: 0,
                    reason: "valid"
                  },
                  id: "5acdd527be26511bee8a82fd",
                  lastModified: "2019-11-29T19:32:36.722000",
                  processing: {
                    meta: {
                      flag: true
                    }
                  },
                  __order: 2
                },
                {
                  Top: {
                    box: [110, 274, 511, 645],
                    style: "tee",
                    neck: "round neck",
                    sleeve: "short",
                    pattern: "printed",
                    length: "mid",
                    pattern_coordinates: [
                      {
                        $numberDouble: "0.21977175772190094"
                      },
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.09170468896627426"
                      },
                      {
                        $numberDouble: "0.05470795929431915"
                      },
                      0,
                      {
                        $numberDouble: "0.3070237934589386"
                      },
                      {
                        $numberDouble: "0.10451771318912506"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.17023314535617828"
                      },
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.14967957139015198"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.7261657118797302"
                      },
                      0,
                      {
                        $numberDouble: "0.1210855096578598"
                      },
                      0,
                      0,
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.42594242095947266"
                      },
                      0,
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.027815818786621094"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.4215777516365051"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.3016986846923828"
                      },
                      {
                        $numberDouble: "0.258819043636322"
                      },
                      {
                        $numberDouble: "0.5344129800796509"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.25890111923217773"
                      },
                      0,
                      {
                        $numberDouble: "0.09935338795185089"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.3556171655654907"
                      },
                      {
                        $numberDouble: "0.7320185303688049"
                      },
                      0,
                      {
                        $numberDouble: "0.7201056480407715"
                      },
                      0,
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.8217256665229797"
                      },
                      {
                        $numberDouble: "0.24202638864517212"
                      },
                      {
                        $numberDouble: "0.19161739945411682"
                      },
                      {
                        $numberDouble: "0.0531473346054554"
                      },
                      0,
                      {
                        $numberDouble: "0.6810094714164734"
                      },
                      0,
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.6317251920700073"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.16057030856609344"
                      },
                      0,
                      {
                        $numberDouble: "0.21716028451919556"
                      },
                      0,
                      {
                        $numberDouble: "0.6894679069519043"
                      },
                      0,
                      {
                        $numberDouble: "0.5800353288650513"
                      },
                      {
                        $numberDouble: "0.4523375332355499"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.3755893409252167"
                      },
                      0,
                      {
                        $numberDouble: "0.3166253864765167"
                      },
                      0,
                      0,
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.2860943078994751"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.7323161363601685"
                      },
                      0,
                      {
                        $numberDouble: "0.49340882897377014"
                      },
                      0,
                      {
                        $numberDouble: "0.012137258425354958"
                      },
                      {
                        $numberDouble: "0.15134191513061523"
                      },
                      0,
                      {
                        $numberDouble: "0.21755413711071014"
                      },
                      {
                        $numberDouble: "0.7974422574043274"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.6319406032562256"
                      },
                      0,
                      {
                        $numberDouble: "0.2963363826274872"
                      },
                      0,
                      0,
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.3108070492744446"
                      },
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.5049089193344116"
                      },
                      {
                        $numberDouble: "0.3511156737804413"
                      },
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.679016649723053"
                      },
                      {
                        $numberDouble: "0.0072885118424892426"
                      },
                      {
                        $numberDouble: "0.5093464851379395"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.40943023562431335"
                      },
                      {
                        $numberDouble: "0.3040130138397217"
                      },
                      0,
                      {
                        $numberDouble: "0.7042996287345886"
                      },
                      {
                        $numberDouble: "0.37494146823883057"
                      },
                      0
                    ],
                    colors: {
                      LAB: [
                        {
                          $numberDouble: "90.47838610971259"
                        },
                        {
                          $numberDouble: "1.9492735967673869"
                        },
                        {
                          $numberDouble: "-1.9162268374359748"
                        }
                      ]
                    },
                    detail: "embroidered",
                    material: "linen"
                  },
                  name: "562316703448421875.jpg",
                  Bottom: {
                    box: [159, 624, 485, 897],
                    colors: {
                      LAB: [
                        {
                          $numberDouble: "95.33701998375368"
                        },
                        {
                          $numberDouble: "0.4297259432690659"
                        },
                        {
                          $numberDouble: "-0.9367841407253996"
                        }
                      ]
                    },
                    type: "Shorts",
                    pattern: "solids",
                    length: "mid",
                    material: "denim",
                    detail: "embroidered"
                  },
                  img_url:
                    "https://i.pinimg.com/736x/b2/7c/e1/b27ce1ae573a5f50c6f5c7478ad5d95a--floral-crop-tops-chiffon-skirt.jpg",
                  verified_tags: {
                    verified_tags: [
                      {
                        top: {
                          style: "tee",
                          neck: "round neck",
                          sleeve: "short",
                          pattern: "printed",
                          material: "cotton",
                          detail: "plain",
                          length: "mid",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "90.56531299151796"
                                },
                                {
                                  $numberDouble: "1.7292916165527195"
                                },
                                {
                                  $numberDouble: "-1.5970663267453666"
                                }
                              ]
                            }
                          }
                        },
                        bottom: {
                          type: "Shorts",
                          pattern: "solids",
                          length: "short",
                          material: "denim",
                          detail: "plain",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "95.26984916311271"
                                },
                                {
                                  $numberDouble: "0.6916889415789496"
                                },
                                {
                                  $numberDouble: "-0.8547713879835328"
                                }
                              ]
                            }
                          }
                        }
                      },
                      {
                        top: {
                          style: "tee",
                          neck: "round neck",
                          sleeve: "short",
                          length: "mid",
                          detail: "plain",
                          pattern: "printed",
                          material: "cotton",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "90.56531299151796"
                                },
                                {
                                  $numberDouble: "1.7292916165527195"
                                },
                                {
                                  $numberDouble: "-1.5970663267453666"
                                }
                              ]
                            }
                          }
                        },
                        bottom: {
                          type: "Shorts",
                          length: "mid",
                          detail: "plain",
                          pattern: "solids",
                          material: "denim",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "95.26984916311271"
                                },
                                {
                                  $numberDouble: "0.6916889415789496"
                                },
                                {
                                  $numberDouble: "-0.8547713879835328"
                                }
                              ]
                            }
                          }
                        },
                        inappropriate: false,
                        swimwear: false,
                        sleepwear: false,
                        Celebrity: false,
                        logo: false,
                        notwoman: false
                      },
                      {
                        top: {
                          style: "tee",
                          sleeve: "short",
                          neck: "not visible",
                          length: "mid",
                          pattern: "floral",
                          material: "linen",
                          detail: "plain",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "90.56531299151796"
                                },
                                {
                                  $numberDouble: "1.7292916165527195"
                                },
                                {
                                  $numberDouble: "-1.5970663267453666"
                                }
                              ]
                            }
                          }
                        },
                        bottom: {
                          type: "Shorts",
                          length: "mid",
                          detail: "embroidered",
                          pattern: "solids",
                          material: "denim",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "95.26984916311271"
                                },
                                {
                                  $numberDouble: "0.6916889415789496"
                                },
                                {
                                  $numberDouble: "-0.8547713879835328"
                                }
                              ]
                            }
                          }
                        },
                        inappropriate: false,
                        swimwear: false,
                        sleepwear: false,
                        Celebrity: false,
                        logo: false,
                        notwoman: false
                      },
                      {
                        top: {
                          style: "tee",
                          neck: "boat neck",
                          sleeve: "short",
                          detail: "plain",
                          pattern: "printed",
                          length: "mid",
                          material: "not sure",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "90.56531299151796"
                                },
                                {
                                  $numberDouble: "1.7292916165527195"
                                },
                                {
                                  $numberDouble: "-1.5970663267453666"
                                }
                              ]
                            }
                          }
                        },
                        bottom: {
                          type: "Shorts",
                          material: "denim",
                          length: "mid",
                          detail: "embroidered",
                          pattern: "solids",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "95.26984916311271"
                                },
                                {
                                  $numberDouble: "0.6916889415789496"
                                },
                                {
                                  $numberDouble: "-0.8547713879835328"
                                }
                              ]
                            }
                          }
                        },
                        inappropriate: false,
                        swimwear: false,
                        sleepwear: false,
                        Celebrity: false,
                        logo: false,
                        notwoman: false
                      },
                      {
                        top: {
                          style: "tee",
                          neck: "not visible",
                          sleeve: "short",
                          length: "mid",
                          pattern: "floral",
                          detail: "plain",
                          material: "not sure",
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "91.73083904543601"
                              },
                              {
                                $numberDouble: "3.558546500732618"
                              },
                              {
                                $numberDouble: "0.7122448128813463"
                              }
                            ]
                          }
                        },
                        bottom: {
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "96.24026104724992"
                              },
                              {
                                $numberDouble: "0.3593715097386929"
                              },
                              {
                                $numberDouble: "-0.9700048734654798"
                              }
                            ]
                          },
                          type: "Shorts",
                          length: "short",
                          detail: "plain",
                          pattern: "solids",
                          material: "not sure"
                        },
                        inappropriate: false,
                        swimwear: false,
                        sleepwear: false,
                        Celebrity: null,
                        logo: false,
                        notwoman: false
                      },
                      {
                        top: {
                          style: "tee",
                          neck: "not visible",
                          sleeve: "short",
                          length: "mid",
                          pattern: "floral",
                          detail: "plain",
                          material: "not sure",
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "91.37937745771218"
                              },
                              {
                                $numberDouble: "3.5615281332706528"
                              },
                              {
                                $numberDouble: "0.7129743639898489"
                              }
                            ]
                          }
                        },
                        bottom: {
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "96.58792803488979"
                              },
                              {
                                $numberDouble: "0.35910815130896223"
                              },
                              {
                                $numberDouble: "-0.9692953740878085"
                              }
                            ]
                          },
                          type: "Shorts",
                          length: "mid",
                          detail: "plain",
                          pattern: "solids",
                          material: "not sure"
                        },
                        inappropriate: false,
                        swimwear: false,
                        sleepwear: false,
                        Celebrity: null,
                        logo: false,
                        notwoman: false
                      },
                      {
                        top: {
                          style: "tee",
                          neck: "not visible",
                          sleeve: "short",
                          length: "mid",
                          pattern: "floral",
                          detail: "plain",
                          material: "not sure",
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "91.37937745771218"
                              },
                              {
                                $numberDouble: "3.5615281332706528"
                              },
                              {
                                $numberDouble: "0.7129743639898489"
                              }
                            ]
                          }
                        },
                        bottom: {
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "94.14855672691253"
                              },
                              {
                                $numberDouble: "0.36097964111597713"
                              },
                              {
                                $numberDouble: "-0.9743347473246677"
                              }
                            ]
                          },
                          type: "Shorts",
                          length: "short",
                          detail: "plain",
                          pattern: "solids",
                          material: "not sure"
                        },
                        inappropriate: false,
                        swimwear: false,
                        sleepwear: false,
                        Celebrity: null,
                        logo: false,
                        notwoman: false
                      },
                      {
                        top: {
                          style: "tee",
                          neck: "not visible",
                          sleeve: "short",
                          length: "mid",
                          pattern: "floral",
                          detail: "plain",
                          material: "not sure",
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "90.47838610971259"
                              },
                              {
                                $numberDouble: "1.9492735967673869"
                              },
                              {
                                $numberDouble: "-1.9162268374359748"
                              }
                            ]
                          }
                        },
                        bottom: {
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "95.33701998375368"
                              },
                              {
                                $numberDouble: "0.4297259432690659"
                              },
                              {
                                $numberDouble: "-0.9367841407253996"
                              }
                            ]
                          },
                          type: "Shorts",
                          length: "mid",
                          detail: "plain",
                          pattern: "solids",
                          material: "not sure"
                        },
                        inappropriate: false,
                        swimwear: false,
                        sleepwear: false,
                        Celebrity: null,
                        logo: false,
                        notwoman: false
                      }
                    ],
                    verified_by: [
                      "5b20795400cbc699af0df1ad",
                      "5b223f982756dd78715370c8",
                      "5b223ff82756dd78715370c9",
                      "5b2240272756dd78715370ca",
                      "5b56ec3800cbc601f5f72c94",
                      "5b56ec1700cbc601f5f72c93",
                      "5b2240112756dd786c4d9691",
                      "5b56ebdb00cbc601f5f72c91"
                    ]
                  },
                  reviewed_tags: {
                    top: {
                      style: "",
                      neck: "",
                      sleeve: "",
                      length: "",
                      pattern: "",
                      detail: "",
                      material: ""
                    },
                    bottom: {
                      ankle: "",
                      fit: "",
                      type: "",
                      length: "",
                      pattern: "",
                      detail: "",
                      material: ""
                    },
                    reviewed_by: ""
                  },
                  classification: {
                    classified: {
                      top: 0,
                      bottom: 0
                    },
                    on_classification: {
                      top: 1,
                      bottom: 1
                    },
                    classified_as: {
                      top: "",
                      bottom: ""
                    }
                  },
                  common: {
                    color: {
                      top: {
                        done: 0,
                        on_process: 0,
                        value: ""
                      },
                      bottom: {
                        done: 0,
                        on_process: 0,
                        value: ""
                      }
                    }
                  },
                  validity: {
                    validated: 1,
                    on_validation: 0,
                    invalid: 0,
                    reason: "valid"
                  },
                  id: "5acdd524be26511bee8a82ed",
                  lastModified: "2019-11-29T19:32:36.722000",
                  processing: {
                    meta: {
                      flag: true
                    }
                  },
                  __order: 3
                },
                {
                  Top: {
                    box: [49, 24, 409, 708],
                    style: "blouse",
                    sleeve: "long",
                    pattern: "printed",
                    material: "satin",
                    detail: "lace",
                    length: "tunic",
                    pattern_coordinates: [
                      {
                        $numberDouble: "0.37485113739967346"
                      },
                      0,
                      {
                        $numberDouble: "0.15923571586608887"
                      },
                      0,
                      {
                        $numberDouble: "0.7692858576774597"
                      },
                      {
                        $numberDouble: "0.11201062053442001"
                      },
                      0,
                      {
                        $numberDouble: "0.16664302349090576"
                      },
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.09236398339271545"
                      },
                      0,
                      {
                        $numberDouble: "0.3801601529121399"
                      },
                      0,
                      {
                        $numberDouble: "0.20629358291625977"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.6160475015640259"
                      },
                      0,
                      {
                        $numberDouble: "0.3647313117980957"
                      },
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.15420737862586975"
                      },
                      0,
                      {
                        $numberDouble: "0.22118091583251953"
                      },
                      0,
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.3511684536933899"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.24141082167625427"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.46898379921913147"
                      },
                      {
                        $numberDouble: "0.22882743179798126"
                      },
                      {
                        $numberDouble: "0.6147753596305847"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.23847949504852295"
                      },
                      0,
                      {
                        $numberDouble: "0.09224953502416611"
                      },
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.8282762765884399"
                      },
                      0,
                      {
                        $numberDouble: "1.0526078939437866"
                      },
                      0,
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.7177398800849915"
                      },
                      {
                        $numberDouble: "0.5551934242248535"
                      },
                      {
                        $numberDouble: "0.21793486177921295"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.6599982976913452"
                      },
                      0,
                      {
                        $numberDouble: "0.29422202706336975"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.6315776705741882"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.19972746074199677"
                      },
                      {
                        $numberDouble: "0.07294943928718567"
                      },
                      {
                        $numberDouble: "0.3960745334625244"
                      },
                      0,
                      {
                        $numberDouble: "0.8235004544258118"
                      },
                      0,
                      {
                        $numberDouble: "0.4667382836341858"
                      },
                      {
                        $numberDouble: "0.2875185012817383"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.40757086873054504"
                      },
                      0,
                      {
                        $numberDouble: "0.417436420917511"
                      },
                      0,
                      {
                        $numberDouble: "0.05716249346733093"
                      },
                      0,
                      {
                        $numberDouble: "0.07040362060070038"
                      },
                      0,
                      {
                        $numberDouble: "0.2503446638584137"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.5133442878723145"
                      },
                      0,
                      {
                        $numberDouble: "0.23109498620033264"
                      },
                      {
                        $numberDouble: "0.572788655757904"
                      },
                      {
                        $numberDouble: "0.20019006729125977"
                      },
                      {
                        $numberDouble: "0.11116111278533936"
                      },
                      0,
                      {
                        $numberDouble: "0.14248284697532654"
                      },
                      {
                        $numberDouble: "0.6231521368026733"
                      },
                      {
                        $numberDouble: "0.45174440741539"
                      },
                      0,
                      {
                        $numberDouble: "0.5687803626060486"
                      },
                      0,
                      {
                        $numberDouble: "0.18017879128456116"
                      },
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.06558368355035782"
                      },
                      {
                        $numberDouble: "0.34201282262802124"
                      },
                      {
                        $numberDouble: "0.27512073516845703"
                      },
                      {
                        $numberDouble: "0.26227667927742004"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.46169513463974"
                      },
                      {
                        $numberDouble: "0.18599596619606018"
                      },
                      {
                        $numberDouble: "0.07967374473810196"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.2332383096218109"
                      },
                      0,
                      {
                        $numberDouble: "0.5888091325759888"
                      },
                      0,
                      {
                        $numberDouble: "0.15558432042598724"
                      },
                      {
                        $numberDouble: "0.4543188214302063"
                      },
                      {
                        $numberDouble: "0.041232623159885406"
                      },
                      0,
                      {
                        $numberDouble: "0.3975328505039215"
                      },
                      {
                        $numberDouble: "0.25738009810447693"
                      },
                      0
                    ],
                    colors: {
                      LAB: [
                        {
                          $numberDouble: "78.71391324569971"
                        },
                        {
                          $numberDouble: "-17.839044012871398"
                        },
                        {
                          $numberDouble: "9.32387993805981"
                        }
                      ]
                    }
                  },
                  name: "258394097347208961.jpg",
                  Bottom: {
                    box: [107, 633, 396, 721],
                    colors: {
                      LAB: [
                        {
                          $numberDouble: "0.27072754378082564"
                        },
                        {
                          $numberDouble: "0.002464983406266774"
                        },
                        {
                          $numberDouble: "0.0039284843139753"
                        }
                      ]
                    },
                    type: "Shorts",
                    pattern: "solids",
                    length: "short"
                  },
                  img_url:
                    "https://i.pinimg.com/736x/bd/27/5e/bd275e9f043ea5136799daff4060d5aa--white-short-outfits-mint-outfits.jpg",
                  verified_tags: {
                    verified_tags: [
                      {
                        top: {
                          style: "blouse",
                          detail: "lace",
                          length: "tunic",
                          material: "satin",
                          pattern: "solids",
                          sleeve: "long",
                          neck: "boat neck",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "78.73328547047686"
                                },
                                {
                                  $numberDouble: "-17.901800630222898"
                                },
                                {
                                  $numberDouble: "9.318471621711755"
                                }
                              ]
                            }
                          }
                        },
                        bottom: {
                          type: "Shorts",
                          pattern: "solids",
                          length: "short",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "0.2741734960237956"
                                },
                                {
                                  $numberDouble: "0.00003730098721566044"
                                },
                                {
                                  $numberDouble: "-0.00007380509088883436"
                                }
                              ]
                            }
                          }
                        },
                        inappropriate: false,
                        swimwear: false,
                        sleepwear: false,
                        Celebrity: false,
                        logo: false,
                        notwoman: false
                      },
                      {
                        top: {
                          style: "blouse",
                          neck: "boat neck",
                          sleeve: "long",
                          length: "tunic",
                          pattern: "printed",
                          detail: "lace",
                          material: "not sure",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "78.73328547047686"
                                },
                                {
                                  $numberDouble: "-17.901800630222898"
                                },
                                {
                                  $numberDouble: "9.318471621711755"
                                }
                              ]
                            }
                          }
                        },
                        bottom: {
                          type: "Shorts",
                          length: "short",
                          detail: "not visible",
                          pattern: "solids",
                          material: "not sure",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "0.2741734960237956"
                                },
                                {
                                  $numberDouble: "0.00003730098721566044"
                                },
                                {
                                  $numberDouble: "-0.00007380509088883436"
                                }
                              ]
                            }
                          }
                        },
                        inappropriate: false,
                        swimwear: false,
                        sleepwear: false,
                        Celebrity: false,
                        logo: false,
                        notwoman: false
                      }
                    ],
                    verified_by: [
                      "5b20795400cbc699af0df1ad",
                      "5b223ff82756dd78715370c9"
                    ]
                  },
                  reviewed_tags: {
                    top: {
                      style: "",
                      neck: "",
                      sleeve: "",
                      length: "",
                      pattern: "",
                      detail: "",
                      material: ""
                    },
                    bottom: {
                      ankle: "",
                      fit: "",
                      type: "",
                      length: "",
                      pattern: "",
                      detail: "",
                      material: ""
                    },
                    reviewed_by: ""
                  },
                  classification: {
                    classified: {
                      top: 0,
                      bottom: 0
                    },
                    on_classification: {
                      top: 1,
                      bottom: 1
                    },
                    classified_as: {
                      top: "",
                      bottom: ""
                    }
                  },
                  common: {
                    color: {
                      top: {
                        done: 0,
                        on_process: 0,
                        value: ""
                      },
                      bottom: {
                        done: 0,
                        on_process: 0,
                        value: ""
                      }
                    }
                  },
                  validity: {
                    validated: 1,
                    on_validation: 0,
                    invalid: 0,
                    reason: "valid"
                  },
                  id: "5acdd525be26511bee8a82f0",
                  lastModified: "2019-11-29T19:32:36.722000",
                  processing: {
                    meta: {
                      flag: true
                    }
                  },
                  __order: 4
                },
                {
                  Top: {
                    box: [268, 33, 430, 241],
                    neck: "scoop neck",
                    sleeve: "sleeveless",
                    pattern: "stripes",
                    material: "cotton",
                    length: "mid",
                    style: "tank",
                    pattern_coordinates: [
                      {
                        $numberDouble: "0.43431755900382996"
                      },
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.09764186292886734"
                      },
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.15225978195667267"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.5074172019958496"
                      },
                      0,
                      {
                        $numberDouble: "0.1899719089269638"
                      },
                      {
                        $numberDouble: "0.11730343103408813"
                      },
                      {
                        $numberDouble: "0.5693678259849548"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.5513216853141785"
                      },
                      0,
                      {
                        $numberDouble: "0.1561681032180786"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.0905972272157669"
                      },
                      0,
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.10140480101108551"
                      },
                      {
                        $numberDouble: "0.41787949204444885"
                      },
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.16361673176288605"
                      },
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.13027682900428772"
                      },
                      0,
                      0,
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.5064845085144043"
                      },
                      0,
                      {
                        $numberDouble: "0.4087355136871338"
                      },
                      0,
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.8750359416007996"
                      },
                      {
                        $numberDouble: "0.3608386814594269"
                      },
                      0,
                      {
                        $numberDouble: "0.636751651763916"
                      },
                      {
                        $numberDouble: "0.05625242739915848"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.18117108941078186"
                      },
                      0,
                      {
                        $numberDouble: "0.23485559225082397"
                      },
                      {
                        $numberDouble: "0.9067551493644714"
                      },
                      {
                        $numberDouble: "0.2615372836589813"
                      },
                      0,
                      {
                        $numberDouble: "0.32703566551208496"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.08702352643013"
                      },
                      {
                        $numberDouble: "0.19406871497631073"
                      },
                      0,
                      {
                        $numberDouble: "0.154942587018013"
                      },
                      0,
                      {
                        $numberDouble: "0.11780469864606857"
                      },
                      0,
                      {
                        $numberDouble: "0.4337995946407318"
                      },
                      0,
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.04560065641999245"
                      },
                      {
                        $numberDouble: "0.05915996804833412"
                      },
                      0,
                      {
                        $numberDouble: "0.08441983163356781"
                      },
                      0,
                      {
                        $numberDouble: "0.06688081473112106"
                      },
                      {
                        $numberDouble: "0.6613868474960327"
                      },
                      {
                        $numberDouble: "0.13111187517642975"
                      },
                      {
                        $numberDouble: "0.6135489344596863"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.36206191778182983"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.4324994683265686"
                      },
                      0,
                      {
                        $numberDouble: "0.44391387701034546"
                      },
                      {
                        $numberDouble: "0.27108028531074524"
                      },
                      0,
                      0,
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.3244049847126007"
                      },
                      0,
                      {
                        $numberDouble: "0.28684136271476746"
                      },
                      {
                        $numberDouble: "0.08967288583517075"
                      },
                      {
                        $numberDouble: "0.09332127869129181"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.8793562054634094"
                      },
                      {
                        $numberDouble: "0.04898678511381149"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.1441604197025299"
                      },
                      {
                        $numberDouble: "0.1630219966173172"
                      },
                      {
                        $numberDouble: "0.23586201667785645"
                      },
                      0,
                      {
                        $numberDouble: "0.04141366109251976"
                      },
                      0,
                      {
                        $numberDouble: "0.5814434885978699"
                      },
                      {
                        $numberDouble: "0.0966518372297287"
                      },
                      {
                        $numberDouble: "0.4219910800457001"
                      },
                      {
                        $numberDouble: "0.08937320113182068"
                      },
                      0
                    ],
                    colors: {
                      LAB: [
                        {
                          $numberDouble: "4.741519214182148"
                        },
                        {
                          $numberDouble: "1.0894493842456832"
                        },
                        {
                          $numberDouble: "-1.2846388580700752"
                        }
                      ]
                    },
                    detail: "plain"
                  },
                  name: "130393351690380771.jpg",
                  Bottom: {
                    box: [212, 216, 446, 773],
                    colors: {
                      LAB: [
                        {
                          $numberDouble: "51.205421938878885"
                        },
                        {
                          $numberDouble: "-1.0046252070643469"
                        },
                        {
                          $numberDouble: "-15.704889483979056"
                        }
                      ]
                    },
                    type: "Pants",
                    fit: "loose",
                    pattern: "solids",
                    material: "denim",
                    detail: "plain",
                    length: "full",
                    ankle: "bootcup"
                  },
                  img_url:
                    "https://i.pinimg.com/736x/03/b1/7e/03b17e0296f592300b7b441742d5d39e--palazzo-flare.jpg",
                  verified_tags: {
                    verified_tags: [
                      {
                        top: {
                          neck: "scoop neck",
                          sleeve: "sleeveless",
                          pattern: "stripes",
                          material: "cotton",
                          detail: "plain",
                          style: "tank",
                          length: "mid",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "4.818387834211304"
                                },
                                {
                                  $numberDouble: "0.7633263497198867"
                                },
                                {
                                  $numberDouble: "-0.9542419737932184"
                                }
                              ]
                            }
                          },
                          hexNum: "111012"
                        },
                        bottom: {
                          type: "Pants",
                          fit: "loose",
                          pattern: "solids",
                          material: "denim",
                          detail: "plain",
                          ankle: "bootcup",
                          length: "full",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "51.26307358350823"
                                },
                                {
                                  $numberDouble: "-1.409475059931664"
                                },
                                {
                                  $numberDouble: "-15.342837110959117"
                                }
                              ]
                            }
                          },
                          hexNum: "687C94"
                        },
                        reject: false
                      },
                      {
                        top: {
                          style: "tank",
                          neck: "scoop neck",
                          sleeve: "sleeveless",
                          length: "mid",
                          pattern: "horizontal_stripes",
                          detail: "plain",
                          material: "cotton",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "4.818387834211304"
                                },
                                {
                                  $numberDouble: "0.7633263497198867"
                                },
                                {
                                  $numberDouble: "-0.9542419737932184"
                                }
                              ]
                            }
                          },
                          hexNum: "111012"
                        },
                        bottom: {
                          type: "Pants",
                          ankle: "bootcup",
                          fit: "loose",
                          length: "full",
                          material: "denim",
                          pattern: "solids",
                          detail: "plain",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "51.26307358350823"
                                },
                                {
                                  $numberDouble: "-1.409475059931664"
                                },
                                {
                                  $numberDouble: "-15.342837110959117"
                                }
                              ]
                            }
                          },
                          hexNum: "687C94"
                        },
                        inappropriate: false,
                        swimwear: false,
                        sleepwear: false,
                        Celebrity: false,
                        logo: false,
                        notwoman: false,
                        reject: false
                      },
                      {
                        top: {
                          style: "tank",
                          neck: "scoop neck",
                          sleeve: "sleeveless",
                          length: "mid",
                          pattern: "horizontal_stripes",
                          detail: "plain",
                          material: "not sure",
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "4.741519214182148"
                              },
                              {
                                $numberDouble: "1.0894493842456832"
                              },
                              {
                                $numberDouble: "-1.2846388580700752"
                              }
                            ]
                          },
                          hexNum: "111012"
                        },
                        bottom: {
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "51.205421938878885"
                              },
                              {
                                $numberDouble: "-1.0046252070643469"
                              },
                              {
                                $numberDouble: "-15.704889483979056"
                              }
                            ]
                          },
                          type: "Pants",
                          ankle: "bootcup",
                          fit: "straight",
                          length: "full",
                          detail: "plain",
                          pattern: "solids",
                          material: "denim",
                          hexNum: "687C94"
                        },
                        inappropriate: false,
                        swimwear: false,
                        sleepwear: false,
                        Celebrity: null,
                        logo: false,
                        notwoman: false,
                        reject: true
                      }
                    ],
                    verified_by: [
                      "5b20795400cbc699af0df1ad",
                      "5b223ff82756dd78715370c9",
                      "5b56ec1700cbc601f5f72c93"
                    ]
                  },
                  reviewed_tags: {
                    top: {
                      style: "",
                      neck: "",
                      sleeve: "",
                      length: "",
                      pattern: "",
                      detail: "",
                      material: ""
                    },
                    bottom: {
                      ankle: "",
                      fit: "",
                      type: "",
                      length: "",
                      pattern: "",
                      detail: "",
                      material: ""
                    },
                    reviewed_by: ""
                  },
                  classification: {
                    classified: {
                      top: 0,
                      bottom: 0
                    },
                    on_classification: {
                      top: 1,
                      bottom: 1
                    },
                    classified_as: {
                      top: "",
                      bottom: ""
                    }
                  },
                  common: {
                    color: {
                      top: {
                        done: 0,
                        on_process: 0,
                        value: ""
                      },
                      bottom: {
                        done: 0,
                        on_process: 0,
                        value: ""
                      }
                    }
                  },
                  validity: {
                    validated: 1,
                    on_validation: 0,
                    invalid: 0,
                    reason: "valid"
                  },
                  id: "5acdd524be26511bee8a82ee",
                  lastModified: "2019-11-29T19:32:36.722000",
                  processing: {
                    meta: {
                      flag: true
                    }
                  },
                  __order: 5
                },
                {
                  Top: {
                    box: [162, 67, 451, 423],
                    style: "tee",
                    neck: "round neck",
                    sleeve: "cap",
                    pattern: "textlogos",
                    material: "cotton",
                    length: "mid",
                    pattern_coordinates: [
                      {
                        $numberDouble: "0.07715452462434769"
                      },
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.07226105779409409"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.3358711898326874"
                      },
                      {
                        $numberDouble: "0.18082135915756226"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.16234973073005676"
                      },
                      0,
                      {
                        $numberDouble: "0.16600646078586578"
                      },
                      0,
                      {
                        $numberDouble: "0.20686344802379608"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.37638652324676514"
                      },
                      0,
                      {
                        $numberDouble: "0.2705690562725067"
                      },
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.27177780866622925"
                      },
                      0,
                      {
                        $numberDouble: "0.38124728202819824"
                      },
                      0,
                      {
                        $numberDouble: "0.028234682977199554"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.5330872535705566"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.18874001502990723"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.6224284768104553"
                      },
                      {
                        $numberDouble: "0.3395361304283142"
                      },
                      {
                        $numberDouble: "0.5496795773506165"
                      },
                      0,
                      {
                        $numberDouble: "0.14534306526184082"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.05173582583665848"
                      },
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.3968769907951355"
                      },
                      0,
                      {
                        $numberDouble: "0.7107694149017334"
                      },
                      {
                        $numberDouble: "0.017117423936724663"
                      },
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.6273946762084961"
                      },
                      {
                        $numberDouble: "0.10983776301145554"
                      },
                      {
                        $numberDouble: "0.3528764247894287"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.6616783142089844"
                      },
                      0,
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.22428587079048157"
                      },
                      {
                        $numberDouble: "0.06222527474164963"
                      },
                      0,
                      {
                        $numberDouble: "0.12809689342975616"
                      },
                      0,
                      {
                        $numberDouble: "0.5960401296615601"
                      },
                      0,
                      {
                        $numberDouble: "0.4942747950553894"
                      },
                      {
                        $numberDouble: "0.18603813648223877"
                      },
                      {
                        $numberDouble: "0.6594109535217285"
                      },
                      {
                        $numberDouble: "0.10915547609329224"
                      },
                      0,
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.1643844097852707"
                      },
                      0,
                      {
                        $numberDouble: "0.1034344807267189"
                      },
                      0,
                      {
                        $numberDouble: "0.06640549004077911"
                      },
                      {
                        $numberDouble: "0.237593874335289"
                      },
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.7733930349349976"
                      },
                      0,
                      {
                        $numberDouble: "0.30969151854515076"
                      },
                      {
                        $numberDouble: "0.056859340518713"
                      },
                      {
                        $numberDouble: "0.15764805674552917"
                      },
                      {
                        $numberDouble: "0.0005555488169193268"
                      },
                      0,
                      {
                        $numberDouble: "0.22501829266548157"
                      },
                      {
                        $numberDouble: "0.6488752365112305"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.2926158607006073"
                      },
                      {
                        $numberDouble: "0.23152491450309753"
                      },
                      {
                        $numberDouble: "0.312244176864624"
                      },
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.1736319661140442"
                      },
                      0,
                      {
                        $numberDouble: "0.5882998704910278"
                      },
                      {
                        $numberDouble: "0.10845600813627243"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.3737078309059143"
                      },
                      {
                        $numberDouble: "0.03352900594472885"
                      },
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.14154842495918274"
                      },
                      0,
                      {
                        $numberDouble: "0.34221574664115906"
                      },
                      0,
                      {
                        $numberDouble: "0.16650766134262085"
                      },
                      {
                        $numberDouble: "0.3100840747356415"
                      },
                      {
                        $numberDouble: "0.2319856584072113"
                      },
                      0,
                      {
                        $numberDouble: "0.34386876225471497"
                      },
                      {
                        $numberDouble: "0.3949706554412842"
                      },
                      0
                    ],
                    colors: {
                      LAB: [
                        {
                          $numberDouble: "95.16216045565515"
                        },
                        {
                          $numberDouble: "-0.45683393838585884"
                        },
                        {
                          $numberDouble: "-3.043442013558839"
                        }
                      ]
                    },
                    detail: "embroidered"
                  },
                  name: "643803709203271895.jpg",
                  Bottom: {
                    box: [181, 398, 375, 508],
                    colors: {
                      LAB: [
                        {
                          $numberDouble: "91.52911404259507"
                        },
                        {
                          $numberDouble: "-1.7447967705546685"
                        },
                        {
                          $numberDouble: "-8.03681067295543"
                        }
                      ]
                    },
                    pattern_coordinates: [
                      {
                        $numberDouble: "0.5519927740097046"
                      },
                      0,
                      {
                        $numberDouble: "0.03621050342917442"
                      },
                      0,
                      {
                        $numberDouble: "0.3462298810482025"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.232038676738739"
                      },
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.420572429895401"
                      },
                      0,
                      {
                        $numberDouble: "0.37530145049095154"
                      },
                      0,
                      {
                        $numberDouble: "0.3331291079521179"
                      },
                      {
                        $numberDouble: "0.31882524490356445"
                      },
                      0,
                      {
                        $numberDouble: "0.1324756145477295"
                      },
                      0,
                      {
                        $numberDouble: "0.43806934356689453"
                      },
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.058507323265075684"
                      },
                      0,
                      {
                        $numberDouble: "0.20982730388641357"
                      },
                      0,
                      {
                        $numberDouble: "0.11496119946241379"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.3992753028869629"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.5303424596786499"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.7718032002449036"
                      },
                      {
                        $numberDouble: "0.30996620655059814"
                      },
                      {
                        $numberDouble: "0.3392091691493988"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.10103687644004822"
                      },
                      0,
                      {
                        $numberDouble: "0.10205933451652527"
                      },
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.7242533564567566"
                      },
                      0,
                      {
                        $numberDouble: "0.8669182658195496"
                      },
                      0,
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.7966931462287903"
                      },
                      {
                        $numberDouble: "0.6422735452651978"
                      },
                      {
                        $numberDouble: "0.09182897210121155"
                      },
                      0,
                      {
                        $numberDouble: "0.07305596768856049"
                      },
                      {
                        $numberDouble: "0.766922652721405"
                      },
                      0,
                      {
                        $numberDouble: "0.377151221036911"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.06835408508777618"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.49821633100509644"
                      },
                      0,
                      {
                        $numberDouble: "0.24929293990135193"
                      },
                      0,
                      {
                        $numberDouble: "0.566658616065979"
                      },
                      {
                        $numberDouble: "0.2235623300075531"
                      },
                      {
                        $numberDouble: "0.36806580424308777"
                      },
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.34559425711631775"
                      },
                      0,
                      {
                        $numberDouble: "0.3022279739379883"
                      },
                      0,
                      {
                        $numberDouble: "0.10100722312927246"
                      },
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.21012945473194122"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.4380749762058258"
                      },
                      0,
                      {
                        $numberDouble: "0.30342036485671997"
                      },
                      {
                        $numberDouble: "0.3950253129005432"
                      },
                      0,
                      {
                        $numberDouble: "0.1954088658094406"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.5273919105529785"
                      },
                      {
                        $numberDouble: "0.14658868312835693"
                      },
                      0,
                      {
                        $numberDouble: "0.6214501857757568"
                      },
                      0,
                      {
                        $numberDouble: "0.14682486653327942"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.04410700127482414"
                      },
                      {
                        $numberDouble: "0.46026864647865295"
                      },
                      {
                        $numberDouble: "0.26979073882102966"
                      },
                      {
                        $numberDouble: "0.230320006608963"
                      },
                      {
                        $numberDouble: "0.3404298424720764"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.2141268402338028"
                      },
                      {
                        $numberDouble: "0.2129044532775879"
                      },
                      {
                        $numberDouble: "0.07200363278388977"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.4190199077129364"
                      },
                      0,
                      {
                        $numberDouble: "0.39361467957496643"
                      },
                      0,
                      {
                        $numberDouble: "0.41763031482696533"
                      },
                      {
                        $numberDouble: "0.2930294871330261"
                      },
                      {
                        $numberDouble: "0.48680993914604187"
                      },
                      0,
                      {
                        $numberDouble: "0.32901236414909363"
                      },
                      {
                        $numberDouble: "0.7133426070213318"
                      },
                      0
                    ],
                    type: "Shorts",
                    pattern: "printed",
                    length: "short",
                    material: "denim"
                  },
                  img_url:
                    "https://i.pinimg.com/736x/64/b9/1d/64b91dc8b5877ea4b46127d55856195a.jpg",
                  verified_tags: {
                    verified_tags: [
                      {
                        top: {
                          style: "tee",
                          neck: "round neck",
                          sleeve: "short",
                          pattern: "textlogos",
                          detail: "plain",
                          length: "mid",
                          material: "cotton",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "95.25525761845954"
                                },
                                {
                                  $numberDouble: "-0.7422291493104916"
                                },
                                {
                                  $numberDouble: "-2.9912511322754076"
                                }
                              ]
                            }
                          }
                        },
                        bottom: {
                          type: "Shorts",
                          pattern: "printed",
                          length: "short",
                          material: "denim",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "91.42719915496693"
                                },
                                {
                                  $numberDouble: "-1.6177901623027902"
                                },
                                {
                                  $numberDouble: "-8.263813007216703"
                                }
                              ]
                            }
                          }
                        }
                      },
                      {
                        top: {
                          style: "tee",
                          neck: "not visible",
                          sleeve: "short",
                          length: "mid",
                          pattern: "graphic",
                          detail: "plain",
                          material: "not sure",
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "96.47644045134727"
                              },
                              {
                                $numberDouble: "0.31929108552908314"
                              },
                              {
                                $numberDouble: "-4.782861878818134"
                              }
                            ]
                          }
                        },
                        bottom: {
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "95.22161590274276"
                              },
                              {
                                $numberDouble: "-2.0009874309079856"
                              },
                              {
                                $numberDouble: "-7.240342287286117"
                              }
                            ]
                          },
                          type: "Shorts",
                          length: "short",
                          detail: "embroidered",
                          pattern: "solids",
                          material: "denim"
                        },
                        inappropriate: false,
                        swimwear: false,
                        sleepwear: false,
                        Celebrity: null,
                        logo: false,
                        notwoman: false
                      }
                    ],
                    verified_by: [
                      "5b20795400cbc699af0df1ad",
                      "5b56ec3800cbc601f5f72c94"
                    ]
                  },
                  reviewed_tags: {
                    top: {
                      style: "",
                      neck: "",
                      sleeve: "",
                      length: "",
                      pattern: "",
                      detail: "",
                      material: ""
                    },
                    bottom: {
                      ankle: "",
                      fit: "",
                      type: "",
                      length: "",
                      pattern: "",
                      detail: "",
                      material: ""
                    },
                    reviewed_by: ""
                  },
                  classification: {
                    classified: {
                      top: 0,
                      bottom: 0
                    },
                    on_classification: {
                      top: 1,
                      bottom: 1
                    },
                    classified_as: {
                      top: "",
                      bottom: ""
                    }
                  },
                  common: {
                    color: {
                      top: {
                        done: 0,
                        on_process: 0,
                        value: ""
                      },
                      bottom: {
                        done: 0,
                        on_process: 0,
                        value: ""
                      }
                    }
                  },
                  validity: {
                    validated: 1,
                    on_validation: 0,
                    invalid: 0,
                    reason: "valid"
                  },
                  id: "5acdd526be26511bee8a82fa",
                  lastModified: "2019-11-29T19:32:36.722000",
                  processing: {
                    meta: {
                      flag: true
                    }
                  },
                  __order: 6
                },
                {
                  Top: {
                    box: [154, 82, 454, 504],
                    style: "blouse",
                    neck: "boat neck",
                    sleeve: "long",
                    pattern: "solids",
                    length: "tunic",
                    colors: {
                      LAB: [
                        {
                          $numberDouble: "65.07862675263024"
                        },
                        {
                          $numberDouble: "8.94322954180804"
                        },
                        {
                          $numberDouble: "-17.9045408358258"
                        }
                      ]
                    },
                    material: "wool",
                    detail: "plain"
                  },
                  name: "262968065721484154.jpg",
                  Bottom: {
                    box: [195, 400, 406, 599],
                    colors: {
                      LAB: [
                        {
                          $numberDouble: "11.399573971235618"
                        },
                        {
                          $numberDouble: "7.256311042757008"
                        },
                        {
                          $numberDouble: "5.361026580219447"
                        }
                      ]
                    },
                    type: "Pants",
                    fit: "pallazo",
                    pattern: "solids",
                    material: "denim",
                    detail: "plain",
                    length: "crop"
                  },
                  img_url:
                    "https://i.pinimg.com/736x/1f/7b/5c/1f7b5c03eee3b6e1b09c808d6ab2d131--crewneck-sweaters-jennifer-lopez.jpg",
                  verified_tags: {
                    verified_tags: [
                      {
                        top: {
                          style: "blouse",
                          neck: "boat neck",
                          sleeve: "long",
                          pattern: "solids",
                          detail: "plain",
                          material: "wool",
                          length: "mid",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "65.04029145580097"
                                },
                                {
                                  $numberDouble: "8.901376573439423"
                                },
                                {
                                  $numberDouble: "-18.05049577671587"
                                }
                              ]
                            }
                          }
                        },
                        bottom: {
                          type: "Pants",
                          fit: "pallazo",
                          length: "crop",
                          pattern: "solids",
                          material: "denim",
                          detail: "plain",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "11.225675797058486"
                                },
                                {
                                  $numberDouble: "7.473323799263132"
                                },
                                {
                                  $numberDouble: "5.3070342027584925"
                                }
                              ]
                            }
                          }
                        }
                      }
                    ],
                    verified_by: ["5b20795400cbc699af0df1ad"]
                  },
                  reviewed_tags: {
                    top: {
                      style: "",
                      neck: "",
                      sleeve: "",
                      length: "",
                      pattern: "",
                      detail: "",
                      material: ""
                    },
                    bottom: {
                      ankle: "",
                      fit: "",
                      type: "",
                      length: "",
                      pattern: "",
                      detail: "",
                      material: ""
                    },
                    reviewed_by: ""
                  },
                  classification: {
                    classified: {
                      top: 1,
                      bottom: 1
                    },
                    on_classification: {
                      top: 0,
                      bottom: 0
                    },
                    classified_as: {
                      top: "NA",
                      bottom: "NA"
                    }
                  },
                  common: {
                    color: {
                      top: {
                        done: 0,
                        on_process: 0,
                        value: ""
                      },
                      bottom: {
                        done: 0,
                        on_process: 0,
                        value: ""
                      }
                    }
                  },
                  validity: {
                    validated: 1,
                    on_validation: 0,
                    invalid: 0,
                    reason: "valid"
                  },
                  id: "5acdd526be26511bee8a82f5",
                  lastModified: "2019-11-29T19:32:36.722000",
                  processing: {
                    meta: {
                      flag: true
                    }
                  },
                  __order: 7
                },
                {
                  Top: {
                    box: [159, 145, 408, 478],
                    style: "blouse",
                    sleeve: "long",
                    pattern: "solids",
                    material: "linen",
                    length: "mid",
                    colors: {
                      LAB: [
                        {
                          $numberDouble: "61.57906443963387"
                        },
                        {
                          $numberDouble: "2.056866952739156"
                        },
                        {
                          $numberDouble: "-6.0131066256519095"
                        }
                      ]
                    },
                    detail: "ruffle"
                  },
                  name: "166492517453518699.jpg",
                  Bottom: {
                    box: [193, 441, 386, 557],
                    colors: {
                      LAB: [
                        {
                          $numberDouble: "33.61366391265129"
                        },
                        {
                          $numberDouble: "7.384559089536724"
                        },
                        {
                          $numberDouble: "6.931453877136818"
                        }
                      ]
                    },
                    type: "Pants",
                    fit: "skinny",
                    pattern: "solids",
                    material: "denim",
                    detail: "distressed",
                    length: "ankle"
                  },
                  img_url:
                    "https://i.pinimg.com/736x/a3/94/2d/a3942d333f76b849c38e964de3e9472b--people-online-lace-inset.jpg",
                  verified_tags: {
                    verified_tags: [
                      {
                        top: {
                          style: "blouse",
                          sleeve: "long",
                          pattern: "solids",
                          material: "wool",
                          detail: "plain",
                          length: "mid",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "61.555784151624025"
                                },
                                {
                                  $numberDouble: "1.8520160381785833"
                                },
                                {
                                  $numberDouble: "-5.907807562609757"
                                }
                              ]
                            }
                          }
                        },
                        bottom: {
                          type: "Pants",
                          fit: "skinny",
                          pattern: "solids",
                          material: "denim",
                          detail: "distressed",
                          length: "ankle",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "33.73676982898108"
                                },
                                {
                                  $numberDouble: "7.256267222080975"
                                },
                                {
                                  $numberDouble: "7.371978484144437"
                                }
                              ]
                            }
                          }
                        },
                        inappropriate: false,
                        swimwear: false,
                        sleepwear: false,
                        Celebrity: false,
                        logo: false,
                        notwoman: false
                      },
                      {
                        top: {
                          style: "blouse",
                          length: "mid",
                          sleeve: "long",
                          pattern: "solids",
                          material: "linen",
                          detail: "ruffle",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "61.555784151624025"
                                },
                                {
                                  $numberDouble: "1.8520160381785833"
                                },
                                {
                                  $numberDouble: "-5.907807562609757"
                                }
                              ]
                            }
                          }
                        },
                        bottom: {
                          type: "Pants",
                          fit: "skinny",
                          length: "ankle",
                          pattern: "solids",
                          material: "denim",
                          detail: "distressed",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "33.73676982898108"
                                },
                                {
                                  $numberDouble: "7.256267222080975"
                                },
                                {
                                  $numberDouble: "7.371978484144437"
                                }
                              ]
                            }
                          }
                        },
                        inappropriate: false,
                        swimwear: false,
                        sleepwear: false,
                        Celebrity: false,
                        logo: false,
                        notwoman: false
                      },
                      {
                        top: {
                          style: "blouse",
                          neck: "boat neck",
                          sleeve: "long",
                          length: "mid",
                          pattern: "abstract",
                          detail: "plain",
                          material: "wool",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "61.555784151624025"
                                },
                                {
                                  $numberDouble: "1.8520160381785833"
                                },
                                {
                                  $numberDouble: "-5.907807562609757"
                                }
                              ]
                            }
                          }
                        },
                        bottom: {
                          type: "Pants",
                          ankle: "tapered",
                          fit: "straight",
                          length: "ankle",
                          detail: "distressed",
                          pattern: "solids",
                          material: "denim",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "33.73676982898108"
                                },
                                {
                                  $numberDouble: "7.256267222080975"
                                },
                                {
                                  $numberDouble: "7.371978484144437"
                                }
                              ]
                            }
                          }
                        },
                        inappropriate: false,
                        swimwear: false,
                        sleepwear: false,
                        Celebrity: false,
                        logo: false,
                        notwoman: false
                      },
                      {
                        top: {
                          style: "blouse",
                          neck: "boat neck",
                          sleeve: "long",
                          length: "mid",
                          pattern: "solids",
                          detail: "ruffle",
                          material: "linen",
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "61.57906443963387"
                              },
                              {
                                $numberDouble: "2.056866952739156"
                              },
                              {
                                $numberDouble: "-6.0131066256519095"
                              }
                            ]
                          }
                        },
                        bottom: {
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "33.61366391265129"
                              },
                              {
                                $numberDouble: "7.384559089536724"
                              },
                              {
                                $numberDouble: "6.931453877136818"
                              }
                            ]
                          },
                          type: "Pants",
                          fit: "skinny",
                          length: "ankle",
                          detail: "distressed",
                          pattern: "solids",
                          material: "denim"
                        },
                        inappropriate: false,
                        swimwear: false,
                        sleepwear: false,
                        Celebrity: null,
                        logo: false,
                        notwoman: false
                      },
                      {
                        top: {
                          style: "tee",
                          neck: "not visible",
                          sleeve: "long",
                          length: "mid",
                          pattern: "NA",
                          detail: "plain",
                          material: "not sure",
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "66.86258713425973"
                              },
                              {
                                $numberDouble: "4.359914639829821"
                              },
                              {
                                $numberDouble: "1.563201934235936"
                              }
                            ]
                          }
                        },
                        bottom: {
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "35.22854554955675"
                              },
                              {
                                $numberDouble: "3.879996781041839"
                              },
                              {
                                $numberDouble: "-15.867975218938168"
                              }
                            ]
                          },
                          type: "NA"
                        },
                        inappropriate: false,
                        swimwear: false,
                        sleepwear: false,
                        Celebrity: null,
                        logo: false,
                        notwoman: false
                      }
                    ],
                    verified_by: [
                      "5b20795400cbc699af0df1ad",
                      "5b20bd6b00cbc6c2d71b3399",
                      "5b213fcd2756dd52114252dc",
                      "5bae112a2756dd08016b0ab6",
                      "5b56ec3800cbc601f5f72c94"
                    ]
                  },
                  reviewed_tags: {
                    top: {
                      style: "",
                      neck: "",
                      sleeve: "",
                      length: "",
                      pattern: "",
                      detail: "",
                      material: ""
                    },
                    bottom: {
                      ankle: "",
                      fit: "",
                      type: "",
                      length: "",
                      pattern: "",
                      detail: "",
                      material: ""
                    },
                    reviewed_by: ""
                  },
                  classification: {
                    classified: {
                      top: 0,
                      bottom: 0
                    },
                    on_classification: {
                      top: 1,
                      bottom: 1
                    },
                    classified_as: {
                      top: "",
                      bottom: ""
                    }
                  },
                  common: {
                    color: {
                      top: {
                        done: 0,
                        on_process: 0,
                        value: ""
                      },
                      bottom: {
                        done: 0,
                        on_process: 0,
                        value: ""
                      }
                    }
                  },
                  validity: {
                    validated: 1,
                    on_validation: 0,
                    invalid: 0,
                    reason: "valid"
                  },
                  id: "5acdd523be26511bee8a82ec",
                  lastModified: "2019-11-29T19:32:36.722000",
                  processing: {
                    meta: {
                      flag: true
                    }
                  },
                  __order: 8
                },
                {
                  Top: {
                    box: [68, 77, 320, 413],
                    style: "blouse",
                    neck: "boat neck",
                    sleeve: "elbow",
                    pattern: "printed",
                    length: "mid",
                    pattern_coordinates: [
                      {
                        $numberDouble: "0.36411598324775696"
                      },
                      0,
                      {
                        $numberDouble: "0.40808647871017456"
                      },
                      0,
                      {
                        $numberDouble: "1.1808682680130005"
                      },
                      {
                        $numberDouble: "0.20115020871162415"
                      },
                      0,
                      {
                        $numberDouble: "0.30915287137031555"
                      },
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.32945114374160767"
                      },
                      0,
                      {
                        $numberDouble: "0.46101757884025574"
                      },
                      0,
                      {
                        $numberDouble: "0.368327260017395"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.6098408699035645"
                      },
                      0,
                      {
                        $numberDouble: "0.6240261793136597"
                      },
                      0,
                      0,
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.16958940029144287"
                      },
                      0,
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.36281508207321167"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.59047532081604"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.5341698527336121"
                      },
                      {
                        $numberDouble: "0.49856698513031006"
                      },
                      {
                        $numberDouble: "0.5218667984008789"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.012412570416927338"
                      },
                      0,
                      {
                        $numberDouble: "0.4580053985118866"
                      },
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.30286705493927"
                      },
                      0,
                      {
                        $numberDouble: "0.4369733929634094"
                      },
                      {
                        $numberDouble: "0.04392947256565094"
                      },
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.852689266204834"
                      },
                      {
                        $numberDouble: "0.36191293597221375"
                      },
                      {
                        $numberDouble: "0.0973595529794693"
                      },
                      {
                        $numberDouble: "0.27977314591407776"
                      },
                      {
                        $numberDouble: "0.5760180950164795"
                      },
                      {
                        $numberDouble: "0.27384451031684875"
                      },
                      0,
                      {
                        $numberDouble: "0.774468183517456"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.5928240418434143"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.5600630044937134"
                      },
                      0,
                      {
                        $numberDouble: "0.013038478791713715"
                      },
                      0,
                      {
                        $numberDouble: "0.4304482936859131"
                      },
                      {
                        $numberDouble: "0.07152202725410461"
                      },
                      {
                        $numberDouble: "0.13391998410224915"
                      },
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.5528119206428528"
                      },
                      0,
                      {
                        $numberDouble: "0.006618056446313858"
                      },
                      0,
                      {
                        $numberDouble: "0.5415216088294983"
                      },
                      0,
                      {
                        $numberDouble: "0.035810571163892746"
                      },
                      0,
                      {
                        $numberDouble: "0.31592655181884766"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.6571616530418396"
                      },
                      0,
                      {
                        $numberDouble: "0.5872007608413696"
                      },
                      {
                        $numberDouble: "0.7962608933448792"
                      },
                      {
                        $numberDouble: "0.05677495151758194"
                      },
                      {
                        $numberDouble: "0.14899298548698425"
                      },
                      0,
                      {
                        $numberDouble: "0.2183251827955246"
                      },
                      {
                        $numberDouble: "0.5667685866355896"
                      },
                      {
                        $numberDouble: "0.6651391386985779"
                      },
                      0,
                      {
                        $numberDouble: "0.5026508569717407"
                      },
                      0,
                      0,
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.3914254903793335"
                      },
                      0,
                      {
                        $numberDouble: "0.3507304787635803"
                      },
                      {
                        $numberDouble: "0.11373720318078995"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.6828901767730713"
                      },
                      {
                        $numberDouble: "0.9409520626068115"
                      },
                      0,
                      0,
                      0,
                      {
                        $numberDouble: "0.478924423456192"
                      },
                      0,
                      {
                        $numberDouble: "0.42365992069244385"
                      },
                      0,
                      {
                        $numberDouble: "0.04234832152724266"
                      },
                      {
                        $numberDouble: "0.13636639714241028"
                      },
                      {
                        $numberDouble: "0.330673485994339"
                      },
                      0,
                      0,
                      {
                        $numberDouble: "0.3357503414154053"
                      },
                      0
                    ],
                    colors: {
                      LAB: [
                        {
                          $numberDouble: "17.87409574222601"
                        },
                        {
                          $numberDouble: "0.07690230801229991"
                        },
                        {
                          $numberDouble: "-0.030104165141364714"
                        }
                      ]
                    },
                    detail: "ruffle"
                  },
                  name: "147281850298115555.jpg",
                  Bottom: {
                    box: [83, 408, 303, 544],
                    type: "Pants",
                    fit: "skinny",
                    pattern: "solids",
                    material: "satin",
                    detail: "plain",
                    length: "ankle",
                    colors: {
                      LAB: [
                        {
                          $numberDouble: "9.149590515802842"
                        },
                        {
                          $numberDouble: "-0.0005312489551323463"
                        },
                        {
                          $numberDouble: "0.0010070027621711386"
                        }
                      ]
                    }
                  },
                  img_url:
                    "https://i.pinimg.com/736x/cf/36/f5/cf36f5ef35447c1592e54051f377322c--cold-shoulder-tops-vince-camuto.jpg",
                  verified_tags: {
                    verified_tags: [
                      {
                        top: {
                          style: "blouse",
                          neck: "boat neck",
                          sleeve: "elbow",
                          detail: "ruffle",
                          pattern: "printed",
                          length: "mid",
                          material: "chiffon",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "18.002902994605904"
                                },
                                {
                                  $numberDouble: "0.0015420023257439741"
                                },
                                {
                                  $numberDouble: "-0.003050935247961295"
                                }
                              ]
                            }
                          },
                          hexNum: "2C2C2C"
                        },
                        bottom: {
                          type: "Pants",
                          ankle: "wide leg",
                          fit: "skinny",
                          length: "ankle",
                          material: "satin",
                          pattern: "solids",
                          detail: "plain",
                          colors: {
                            colors: {
                              LAB: [
                                {
                                  $numberDouble: "9.263234285789427"
                                },
                                {
                                  $numberDouble: "0.0011456658871311642"
                                },
                                {
                                  $numberDouble: "-0.002266762104763398"
                                }
                              ]
                            }
                          },
                          hexNum: "1A1A1A"
                        },
                        inappropriate: false,
                        swimwear: false,
                        sleepwear: false,
                        Celebrity: false,
                        logo: false,
                        notwoman: false,
                        reject: true
                      },
                      {
                        top: {
                          style: "blouse",
                          neck: "round neck",
                          sleeve: "short",
                          length: "mid",
                          pattern: "NA",
                          detail: "plain",
                          material: "not sure",
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "14.679642024623107"
                              },
                              {
                                $numberDouble: "0.0013912953068240252"
                              },
                              {
                                $numberDouble: "-0.002752753235890637"
                              }
                            ]
                          },
                          hexNum: "252525"
                        },
                        bottom: {
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "7.247286038154382"
                              },
                              {
                                $numberDouble: "0.0009859848881799138"
                              },
                              {
                                $numberDouble: "-0.001950905585285101"
                              }
                            ]
                          },
                          type: "NA",
                          hexNum: "161616"
                        },
                        inappropriate: false,
                        swimwear: false,
                        sleepwear: false,
                        Celebrity: null,
                        logo: false,
                        notwoman: false,
                        reject: true
                      },
                      {
                        top: {
                          style: "blouse",
                          neck: "boat neck",
                          sleeve: "short",
                          length: "mid",
                          pattern: "printed",
                          detail: "plain",
                          material: "other",
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "17.87409574222601"
                              },
                              {
                                $numberDouble: "0.07690230801229991"
                              },
                              {
                                $numberDouble: "-0.030104165141364714"
                              }
                            ]
                          },
                          hexNum: "2C2C2C"
                        },
                        bottom: {
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "7.739519325797062"
                              },
                              {
                                $numberDouble: "0.0010529526579677873"
                              },
                              {
                                $numberDouble: "-0.002083410451941825"
                              }
                            ]
                          },
                          type: "NA",
                          hexNum: "171717"
                        },
                        inappropriate: false,
                        swimwear: false,
                        sleepwear: false,
                        Celebrity: null,
                        logo: false,
                        notwoman: false,
                        reject: true
                      },
                      {
                        top: {
                          style: "blouse",
                          neck: "boat neck",
                          sleeve: "elbow",
                          length: "mid",
                          pattern: "printed",
                          detail: "plain",
                          material: "other",
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "17.87409574222601"
                              },
                              {
                                $numberDouble: "0.07690230801229991"
                              },
                              {
                                $numberDouble: "-0.030104165141364714"
                              }
                            ]
                          }
                        },
                        bottom: {
                          colors: {
                            LAB: [
                              {
                                $numberDouble: "9.149590515802842"
                              },
                              {
                                $numberDouble: "-0.0005312489551323463"
                              },
                              {
                                $numberDouble: "0.0010070027621711386"
                              }
                            ]
                          },
                          type: "NA"
                        },
                        inappropriate: false,
                        swimwear: false,
                        sleepwear: false,
                        Celebrity: null,
                        logo: false,
                        notwoman: false
                      }
                    ],
                    verified_by: [
                      "5b20795400cbc699af0df1ad",
                      "5b56ec3800cbc601f5f72c94",
                      "5b2240112756dd786c4d9691",
                      "5b2240272756dd78715370ca"
                    ]
                  },
                  reviewed_tags: {
                    top: {
                      style: "",
                      neck: "",
                      sleeve: "",
                      length: "",
                      pattern: "",
                      detail: "",
                      material: ""
                    },
                    bottom: {
                      ankle: "",
                      fit: "",
                      type: "",
                      length: "",
                      pattern: "",
                      detail: "",
                      material: ""
                    },
                    reviewed_by: ""
                  },
                  classification: {
                    classified: {
                      top: 1,
                      bottom: 1
                    },
                    on_classification: {
                      top: 0,
                      bottom: 0
                    },
                    classified_as: {
                      top: "blouse",
                      bottom: "NA"
                    }
                  },
                  common: {
                    color: {
                      top: {
                        done: 0,
                        on_process: 1,
                        value: ""
                      },
                      bottom: {
                        done: 0,
                        on_process: 0,
                        value: ""
                      }
                    }
                  },
                  validity: {
                    validated: 1,
                    on_validation: 0,
                    invalid: 0,
                    reason: "valid"
                  },
                  id: "5acdd525be26511bee8a82f4",
                  lastModified: "2019-11-29T19:32:36.722000",
                  processing: {
                    meta: {
                      flag: true
                    }
                  },
                  __order: 9
                }
              ],
              IDs: [
                "5acdd525be26511bee8a82f1N0",
                "5acdd526be26511bee8a82f6N0",
                "5acdd527be26511bee8a82fdN0",
                "5acdd524be26511bee8a82edN0",
                "5acdd525be26511bee8a82f0N0",
                "5acdd524be26511bee8a82eeN0",
                "5acdd526be26511bee8a82faN0",
                "5acdd526be26511bee8a82f5N0",
                "5acdd523be26511bee8a82ecN0",
                "5acdd525be26511bee8a82f4N0"
              ]
            },
            meta: {
              vendor: "VERA",
              category: "top",
              version: {
                top: {
                  retinanet: "1",
                  embedding: "4",
                  deeplab: "1"
                }
              },
              ann_algorithm: "annoy"
            }
          }
        }
      },
      meta: {
        features: {
          source: "SAMPLE",
          category: ["top"],
          version: {
            top: {
              retinanet: "1",
              embedding: "4",
              deeplab: "1"
            }
          },
          output_type: "document",
          verbose: false
        }
      }
    }
  },
  social_recommendations: {
    top: {
      "0": {
        similar_data: {
          bottom: {
            "0": {
              similar_items: {
                data: [
                  {
                    id: "53017049",
                    title:
                      "Women's Dolphin Hem Shorts - Mossimo Supply Co.&#153; Olive XS",
                    class: "BOTTOMS",
                    category: "fashion shorts",
                    redirect_url:
                      "https://www.target.com/p/women-s-dolphin-hem-shorts-mossimo-supply-co-153-olive-xs/-/A-53017049",
                    upc: "490170406281",
                    __order: 0,
                    image_url:
                      "https://target.scene7.com/is/image/Target/53017049"
                  },
                  {
                    id: "52473803",
                    title:
                      "Women's Bootcut Curvy Bi-Stretch Twill Pants - A New Day&#153; Gray 16S",
                    class: "BOTTOMS",
                    category: "Trousers",
                    redirect_url:
                      "https://www.target.com/p/women-s-bootcut-curvy-bi-stretch-twill-pants-a-new-day-153-gray-16s/-/A-52473803",
                    upc: "490180459130",
                    __order: 1,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_b45d5ebb-f37d-40a0-a4bb-4755bf2b4d0e"
                  },
                  {
                    id: "53193348",
                    title:
                      "Women's Plus Size Striped Chambray Pants - Universal Thread&#153; Blue 20W",
                    class: "BOTTOMS",
                    category: "fashion pants",
                    redirect_url:
                      "https://www.target.com/p/women-s-plus-size-striped-chambray-pants-universal-thread-153-blue-20w/-/A-53193348",
                    upc: "490211324260",
                    __order: 2,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_f709a868-cab0-40dd-9eef-559dbdd3881b"
                  },
                  {
                    id: "53017039",
                    title:
                      "Women's Striped Dolphin Hem Shorts - Mossimo Supply Co.&#153; Blue M",
                    class: "BOTTOMS",
                    category: "fashion shorts",
                    redirect_url:
                      "https://www.target.com/p/women-s-striped-dolphin-hem-shorts-mossimo-supply-co-153-blue-m/-/A-53017039",
                    upc: "490170405567",
                    __order: 3,
                    image_url:
                      "https://target.scene7.com/is/image/Target/53017039"
                  },
                  {
                    id: "53017106",
                    title:
                      "Women's Floral Ruffle Shorts - Mossimo Supply Co.&#153; Blue XL",
                    class: "BOTTOMS",
                    category: "fashion shorts",
                    redirect_url:
                      "https://www.target.com/p/women-s-floral-ruffle-shorts-mossimo-supply-co-153-blue-xl/-/A-53017106",
                    upc: "490170411117",
                    __order: 4,
                    image_url:
                      "https://target.scene7.com/is/image/Target/53017106"
                  },
                  {
                    id: "52473829",
                    title:
                      "Women's Bootcut Curvy Bi-Stretch Twill Pants - A New Day&#153; Khaki 10S",
                    class: "BOTTOMS",
                    category: "Trousers",
                    redirect_url:
                      "https://www.target.com/p/women-s-bootcut-curvy-bi-stretch-twill-pants-a-new-day-153-khaki-10s/-/A-52473829",
                    upc: "490180459253",
                    __order: 5,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_4a1315d1-a078-42e5-93f2-8bfb9cc45665"
                  },
                  {
                    id: "52821290",
                    title:
                      "Women's High-Rise Raw Hem Straight Jeans - Universal Thread&#153; Light Wash 8 Long",
                    class: "BOTTOMS",
                    category: "Jeans",
                    redirect_url:
                      "https://www.target.com/p/women-s-high-rise-raw-hem-straight-jeans-universal-thread-153-light-wash-8-long/-/A-52821290",
                    upc: "490141157570",
                    __order: 6,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_259f1a97-62b2-4a3e-9002-ce668254b52b"
                  },
                  {
                    id: "52340452",
                    title:
                      "Women's Plus Size Frilled Hem Trouser - Who What Wear&#153; Black 22W",
                    class: "BOTTOMS",
                    category: "Trousers",
                    redirect_url:
                      "https://www.target.com/p/women-s-plus-size-frilled-hem-trouser-who-what-wear-153-black-22w/-/A-52340452",
                    upc: "490211037627",
                    __order: 7,
                    image_url:
                      "https://target.scene7.com/is/image/Target/52340452"
                  }
                ],
                IDs: [
                  "53017049N0",
                  "52473803N0",
                  "53193348N0",
                  "53017039N0",
                  "53017106N0",
                  "52473829N0",
                  "52821290N0",
                  "52340452N0"
                ]
              },
              meta: {
                vendor: "SAMPLE",
                category: "bottom",
                version: {
                  bottom: {
                    retinanet: "1",
                    embedding: "1"
                  }
                },
                ann_algorithm: "annoy"
              }
            }
          }
        },
        meta: {
          features: {
            source: "VERA",
            category: ["top", "bottom"],
            version: {
              top: {
                retinanet: "1",
                embedding: "4",
                deeplab: "1"
              },
              bottom: {
                retinanet: "1",
                embedding: "1"
              }
            },
            output_type: "document",
            verbose: false
          }
        }
      },
      "1": {
        similar_data: {
          bottom: {
            "0": {
              similar_items: {
                data: [
                  {
                    id: "52473803",
                    title:
                      "Women's Bootcut Curvy Bi-Stretch Twill Pants - A New Day&#153; Gray 16S",
                    class: "BOTTOMS",
                    category: "Trousers",
                    redirect_url:
                      "https://www.target.com/p/women-s-bootcut-curvy-bi-stretch-twill-pants-a-new-day-153-gray-16s/-/A-52473803",
                    upc: "490180459130",
                    __order: 0,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_b45d5ebb-f37d-40a0-a4bb-4755bf2b4d0e"
                  },
                  {
                    id: "53017049",
                    title:
                      "Women's Dolphin Hem Shorts - Mossimo Supply Co.&#153; Olive XS",
                    class: "BOTTOMS",
                    category: "fashion shorts",
                    redirect_url:
                      "https://www.target.com/p/women-s-dolphin-hem-shorts-mossimo-supply-co-153-olive-xs/-/A-53017049",
                    upc: "490170406281",
                    __order: 1,
                    image_url:
                      "https://target.scene7.com/is/image/Target/53017049"
                  },
                  {
                    id: "53017039",
                    title:
                      "Women's Striped Dolphin Hem Shorts - Mossimo Supply Co.&#153; Blue M",
                    class: "BOTTOMS",
                    category: "fashion shorts",
                    redirect_url:
                      "https://www.target.com/p/women-s-striped-dolphin-hem-shorts-mossimo-supply-co-153-blue-m/-/A-53017039",
                    upc: "490170405567",
                    __order: 2,
                    image_url:
                      "https://target.scene7.com/is/image/Target/53017039"
                  },
                  {
                    id: "53017106",
                    title:
                      "Women's Floral Ruffle Shorts - Mossimo Supply Co.&#153; Blue XL",
                    class: "BOTTOMS",
                    category: "fashion shorts",
                    redirect_url:
                      "https://www.target.com/p/women-s-floral-ruffle-shorts-mossimo-supply-co-153-blue-xl/-/A-53017106",
                    upc: "490170411117",
                    __order: 3,
                    image_url:
                      "https://target.scene7.com/is/image/Target/53017106"
                  },
                  {
                    id: "52473829",
                    title:
                      "Women's Bootcut Curvy Bi-Stretch Twill Pants - A New Day&#153; Khaki 10S",
                    class: "BOTTOMS",
                    category: "Trousers",
                    redirect_url:
                      "https://www.target.com/p/women-s-bootcut-curvy-bi-stretch-twill-pants-a-new-day-153-khaki-10s/-/A-52473829",
                    upc: "490180459253",
                    __order: 4,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_4a1315d1-a078-42e5-93f2-8bfb9cc45665"
                  },
                  {
                    id: "52340452",
                    title:
                      "Women's Plus Size Frilled Hem Trouser - Who What Wear&#153; Black 22W",
                    class: "BOTTOMS",
                    category: "Trousers",
                    redirect_url:
                      "https://www.target.com/p/women-s-plus-size-frilled-hem-trouser-who-what-wear-153-black-22w/-/A-52340452",
                    upc: "490211037627",
                    __order: 5,
                    image_url:
                      "https://target.scene7.com/is/image/Target/52340452"
                  },
                  {
                    id: "52821290",
                    title:
                      "Women's High-Rise Raw Hem Straight Jeans - Universal Thread&#153; Light Wash 8 Long",
                    class: "BOTTOMS",
                    category: "Jeans",
                    redirect_url:
                      "https://www.target.com/p/women-s-high-rise-raw-hem-straight-jeans-universal-thread-153-light-wash-8-long/-/A-52821290",
                    upc: "490141157570",
                    __order: 6,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_259f1a97-62b2-4a3e-9002-ce668254b52b"
                  },
                  {
                    id: "53193348",
                    title:
                      "Women's Plus Size Striped Chambray Pants - Universal Thread&#153; Blue 20W",
                    class: "BOTTOMS",
                    category: "fashion pants",
                    redirect_url:
                      "https://www.target.com/p/women-s-plus-size-striped-chambray-pants-universal-thread-153-blue-20w/-/A-53193348",
                    upc: "490211324260",
                    __order: 7,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_f709a868-cab0-40dd-9eef-559dbdd3881b"
                  }
                ],
                IDs: [
                  "52473803N0",
                  "53017049N0",
                  "53017039N0",
                  "53017106N0",
                  "52473829N0",
                  "52340452N0",
                  "52821290N0",
                  "53193348N0"
                ]
              },
              meta: {
                vendor: "SAMPLE",
                category: "bottom",
                version: {
                  bottom: {
                    retinanet: "1",
                    embedding: "1"
                  }
                },
                ann_algorithm: "annoy"
              }
            }
          }
        },
        meta: {
          features: {
            source: "VERA",
            category: ["top", "bottom"],
            version: {
              top: {
                retinanet: "1",
                embedding: "4",
                deeplab: "1"
              },
              bottom: {
                retinanet: "1",
                embedding: "1"
              }
            },
            output_type: "document",
            verbose: false
          }
        }
      },
      "2": {
        similar_data: {
          bottom: {
            "0": {
              similar_items: {
                data: [
                  {
                    id: "52473803",
                    title:
                      "Women's Bootcut Curvy Bi-Stretch Twill Pants - A New Day&#153; Gray 16S",
                    class: "BOTTOMS",
                    category: "Trousers",
                    redirect_url:
                      "https://www.target.com/p/women-s-bootcut-curvy-bi-stretch-twill-pants-a-new-day-153-gray-16s/-/A-52473803",
                    upc: "490180459130",
                    __order: 0,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_b45d5ebb-f37d-40a0-a4bb-4755bf2b4d0e"
                  },
                  {
                    id: "53017106",
                    title:
                      "Women's Floral Ruffle Shorts - Mossimo Supply Co.&#153; Blue XL",
                    class: "BOTTOMS",
                    category: "fashion shorts",
                    redirect_url:
                      "https://www.target.com/p/women-s-floral-ruffle-shorts-mossimo-supply-co-153-blue-xl/-/A-53017106",
                    upc: "490170411117",
                    __order: 1,
                    image_url:
                      "https://target.scene7.com/is/image/Target/53017106"
                  },
                  {
                    id: "53017049",
                    title:
                      "Women's Dolphin Hem Shorts - Mossimo Supply Co.&#153; Olive XS",
                    class: "BOTTOMS",
                    category: "fashion shorts",
                    redirect_url:
                      "https://www.target.com/p/women-s-dolphin-hem-shorts-mossimo-supply-co-153-olive-xs/-/A-53017049",
                    upc: "490170406281",
                    __order: 2,
                    image_url:
                      "https://target.scene7.com/is/image/Target/53017049"
                  },
                  {
                    id: "53193348",
                    title:
                      "Women's Plus Size Striped Chambray Pants - Universal Thread&#153; Blue 20W",
                    class: "BOTTOMS",
                    category: "fashion pants",
                    redirect_url:
                      "https://www.target.com/p/women-s-plus-size-striped-chambray-pants-universal-thread-153-blue-20w/-/A-53193348",
                    upc: "490211324260",
                    __order: 3,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_f709a868-cab0-40dd-9eef-559dbdd3881b"
                  },
                  {
                    id: "52473829",
                    title:
                      "Women's Bootcut Curvy Bi-Stretch Twill Pants - A New Day&#153; Khaki 10S",
                    class: "BOTTOMS",
                    category: "Trousers",
                    redirect_url:
                      "https://www.target.com/p/women-s-bootcut-curvy-bi-stretch-twill-pants-a-new-day-153-khaki-10s/-/A-52473829",
                    upc: "490180459253",
                    __order: 4,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_4a1315d1-a078-42e5-93f2-8bfb9cc45665"
                  },
                  {
                    id: "52340452",
                    title:
                      "Women's Plus Size Frilled Hem Trouser - Who What Wear&#153; Black 22W",
                    class: "BOTTOMS",
                    category: "Trousers",
                    redirect_url:
                      "https://www.target.com/p/women-s-plus-size-frilled-hem-trouser-who-what-wear-153-black-22w/-/A-52340452",
                    upc: "490211037627",
                    __order: 5,
                    image_url:
                      "https://target.scene7.com/is/image/Target/52340452"
                  },
                  {
                    id: "52821290",
                    title:
                      "Women's High-Rise Raw Hem Straight Jeans - Universal Thread&#153; Light Wash 8 Long",
                    class: "BOTTOMS",
                    category: "Jeans",
                    redirect_url:
                      "https://www.target.com/p/women-s-high-rise-raw-hem-straight-jeans-universal-thread-153-light-wash-8-long/-/A-52821290",
                    upc: "490141157570",
                    __order: 6,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_259f1a97-62b2-4a3e-9002-ce668254b52b"
                  },
                  {
                    id: "53017039",
                    title:
                      "Women's Striped Dolphin Hem Shorts - Mossimo Supply Co.&#153; Blue M",
                    class: "BOTTOMS",
                    category: "fashion shorts",
                    redirect_url:
                      "https://www.target.com/p/women-s-striped-dolphin-hem-shorts-mossimo-supply-co-153-blue-m/-/A-53017039",
                    upc: "490170405567",
                    __order: 7,
                    image_url:
                      "https://target.scene7.com/is/image/Target/53017039"
                  }
                ],
                IDs: [
                  "52473803N0",
                  "53017106N0",
                  "53017049N0",
                  "53193348N0",
                  "52473829N0",
                  "52340452N0",
                  "52821290N0",
                  "53017039N0"
                ]
              },
              meta: {
                vendor: "SAMPLE",
                category: "bottom",
                version: {
                  bottom: {
                    retinanet: "1",
                    embedding: "1"
                  }
                },
                ann_algorithm: "annoy"
              }
            }
          }
        },
        meta: {
          features: {
            source: "VERA",
            category: ["top", "bottom"],
            version: {
              top: {
                retinanet: "1",
                embedding: "4",
                deeplab: "1"
              },
              bottom: {
                retinanet: "1",
                embedding: "1"
              }
            },
            output_type: "document",
            verbose: false
          }
        }
      },
      "3": {
        similar_data: {
          bottom: {
            "0": {
              similar_items: {
                data: [
                  {
                    id: "53017049",
                    title:
                      "Women's Dolphin Hem Shorts - Mossimo Supply Co.&#153; Olive XS",
                    class: "BOTTOMS",
                    category: "fashion shorts",
                    redirect_url:
                      "https://www.target.com/p/women-s-dolphin-hem-shorts-mossimo-supply-co-153-olive-xs/-/A-53017049",
                    upc: "490170406281",
                    __order: 0,
                    image_url:
                      "https://target.scene7.com/is/image/Target/53017049"
                  },
                  {
                    id: "52340452",
                    title:
                      "Women's Plus Size Frilled Hem Trouser - Who What Wear&#153; Black 22W",
                    class: "BOTTOMS",
                    category: "Trousers",
                    redirect_url:
                      "https://www.target.com/p/women-s-plus-size-frilled-hem-trouser-who-what-wear-153-black-22w/-/A-52340452",
                    upc: "490211037627",
                    __order: 1,
                    image_url:
                      "https://target.scene7.com/is/image/Target/52340452"
                  },
                  {
                    id: "52473829",
                    title:
                      "Women's Bootcut Curvy Bi-Stretch Twill Pants - A New Day&#153; Khaki 10S",
                    class: "BOTTOMS",
                    category: "Trousers",
                    redirect_url:
                      "https://www.target.com/p/women-s-bootcut-curvy-bi-stretch-twill-pants-a-new-day-153-khaki-10s/-/A-52473829",
                    upc: "490180459253",
                    __order: 2,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_4a1315d1-a078-42e5-93f2-8bfb9cc45665"
                  },
                  {
                    id: "53193348",
                    title:
                      "Women's Plus Size Striped Chambray Pants - Universal Thread&#153; Blue 20W",
                    class: "BOTTOMS",
                    category: "fashion pants",
                    redirect_url:
                      "https://www.target.com/p/women-s-plus-size-striped-chambray-pants-universal-thread-153-blue-20w/-/A-53193348",
                    upc: "490211324260",
                    __order: 3,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_f709a868-cab0-40dd-9eef-559dbdd3881b"
                  },
                  {
                    id: "53017039",
                    title:
                      "Women's Striped Dolphin Hem Shorts - Mossimo Supply Co.&#153; Blue M",
                    class: "BOTTOMS",
                    category: "fashion shorts",
                    redirect_url:
                      "https://www.target.com/p/women-s-striped-dolphin-hem-shorts-mossimo-supply-co-153-blue-m/-/A-53017039",
                    upc: "490170405567",
                    __order: 4,
                    image_url:
                      "https://target.scene7.com/is/image/Target/53017039"
                  },
                  {
                    id: "52821290",
                    title:
                      "Women's High-Rise Raw Hem Straight Jeans - Universal Thread&#153; Light Wash 8 Long",
                    class: "BOTTOMS",
                    category: "Jeans",
                    redirect_url:
                      "https://www.target.com/p/women-s-high-rise-raw-hem-straight-jeans-universal-thread-153-light-wash-8-long/-/A-52821290",
                    upc: "490141157570",
                    __order: 5,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_259f1a97-62b2-4a3e-9002-ce668254b52b"
                  },
                  {
                    id: "52473803",
                    title:
                      "Women's Bootcut Curvy Bi-Stretch Twill Pants - A New Day&#153; Gray 16S",
                    class: "BOTTOMS",
                    category: "Trousers",
                    redirect_url:
                      "https://www.target.com/p/women-s-bootcut-curvy-bi-stretch-twill-pants-a-new-day-153-gray-16s/-/A-52473803",
                    upc: "490180459130",
                    __order: 6,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_b45d5ebb-f37d-40a0-a4bb-4755bf2b4d0e"
                  },
                  {
                    id: "53017106",
                    title:
                      "Women's Floral Ruffle Shorts - Mossimo Supply Co.&#153; Blue XL",
                    class: "BOTTOMS",
                    category: "fashion shorts",
                    redirect_url:
                      "https://www.target.com/p/women-s-floral-ruffle-shorts-mossimo-supply-co-153-blue-xl/-/A-53017106",
                    upc: "490170411117",
                    __order: 7,
                    image_url:
                      "https://target.scene7.com/is/image/Target/53017106"
                  }
                ],
                IDs: [
                  "53017049N0",
                  "52340452N0",
                  "52473829N0",
                  "53193348N0",
                  "53017039N0",
                  "52821290N0",
                  "52473803N0",
                  "53017106N0"
                ]
              },
              meta: {
                vendor: "SAMPLE",
                category: "bottom",
                version: {
                  bottom: {
                    retinanet: "1",
                    embedding: "1"
                  }
                },
                ann_algorithm: "annoy"
              }
            }
          }
        },
        meta: {
          features: {
            source: "VERA",
            category: ["top", "bottom"],
            version: {
              top: {
                retinanet: "1",
                embedding: "4",
                deeplab: "1"
              },
              bottom: {
                retinanet: "1",
                embedding: "1"
              }
            },
            output_type: "document",
            verbose: false
          }
        }
      },
      "4": {
        similar_data: {
          bottom: {
            "0": {
              similar_items: {
                message: "string indices must be integers"
              },
              meta: {
                vendor: "SAMPLE",
                category: "bottom",
                version: {
                  bottom: {
                    retinanet: "1",
                    embedding: "1"
                  }
                },
                ann_algorithm: "annoy"
              }
            }
          }
        },
        meta: {
          features: {
            source: "VERA",
            category: ["top", "bottom"],
            version: {
              top: {
                retinanet: "1",
                embedding: "4",
                deeplab: "1"
              },
              bottom: {
                retinanet: "1",
                embedding: "1"
              }
            },
            output_type: "document",
            verbose: false
          }
        }
      },
      "5": {
        similar_data: {
          bottom: {
            "0": {
              similar_items: {
                data: [
                  {
                    id: "53193348",
                    title:
                      "Women's Plus Size Striped Chambray Pants - Universal Thread&#153; Blue 20W",
                    class: "BOTTOMS",
                    category: "fashion pants",
                    redirect_url:
                      "https://www.target.com/p/women-s-plus-size-striped-chambray-pants-universal-thread-153-blue-20w/-/A-53193348",
                    upc: "490211324260",
                    __order: 0,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_f709a868-cab0-40dd-9eef-559dbdd3881b"
                  },
                  {
                    id: "52821290",
                    title:
                      "Women's High-Rise Raw Hem Straight Jeans - Universal Thread&#153; Light Wash 8 Long",
                    class: "BOTTOMS",
                    category: "Jeans",
                    redirect_url:
                      "https://www.target.com/p/women-s-high-rise-raw-hem-straight-jeans-universal-thread-153-light-wash-8-long/-/A-52821290",
                    upc: "490141157570",
                    __order: 1,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_259f1a97-62b2-4a3e-9002-ce668254b52b"
                  },
                  {
                    id: "53017106",
                    title:
                      "Women's Floral Ruffle Shorts - Mossimo Supply Co.&#153; Blue XL",
                    class: "BOTTOMS",
                    category: "fashion shorts",
                    redirect_url:
                      "https://www.target.com/p/women-s-floral-ruffle-shorts-mossimo-supply-co-153-blue-xl/-/A-53017106",
                    upc: "490170411117",
                    __order: 2,
                    image_url:
                      "https://target.scene7.com/is/image/Target/53017106"
                  },
                  {
                    id: "53017039",
                    title:
                      "Women's Striped Dolphin Hem Shorts - Mossimo Supply Co.&#153; Blue M",
                    class: "BOTTOMS",
                    category: "fashion shorts",
                    redirect_url:
                      "https://www.target.com/p/women-s-striped-dolphin-hem-shorts-mossimo-supply-co-153-blue-m/-/A-53017039",
                    upc: "490170405567",
                    __order: 3,
                    image_url:
                      "https://target.scene7.com/is/image/Target/53017039"
                  },
                  {
                    id: "52473829",
                    title:
                      "Women's Bootcut Curvy Bi-Stretch Twill Pants - A New Day&#153; Khaki 10S",
                    class: "BOTTOMS",
                    category: "Trousers",
                    redirect_url:
                      "https://www.target.com/p/women-s-bootcut-curvy-bi-stretch-twill-pants-a-new-day-153-khaki-10s/-/A-52473829",
                    upc: "490180459253",
                    __order: 4,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_4a1315d1-a078-42e5-93f2-8bfb9cc45665"
                  },
                  {
                    id: "52473803",
                    title:
                      "Women's Bootcut Curvy Bi-Stretch Twill Pants - A New Day&#153; Gray 16S",
                    class: "BOTTOMS",
                    category: "Trousers",
                    redirect_url:
                      "https://www.target.com/p/women-s-bootcut-curvy-bi-stretch-twill-pants-a-new-day-153-gray-16s/-/A-52473803",
                    upc: "490180459130",
                    __order: 5,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_b45d5ebb-f37d-40a0-a4bb-4755bf2b4d0e"
                  },
                  {
                    id: "52340452",
                    title:
                      "Women's Plus Size Frilled Hem Trouser - Who What Wear&#153; Black 22W",
                    class: "BOTTOMS",
                    category: "Trousers",
                    redirect_url:
                      "https://www.target.com/p/women-s-plus-size-frilled-hem-trouser-who-what-wear-153-black-22w/-/A-52340452",
                    upc: "490211037627",
                    __order: 6,
                    image_url:
                      "https://target.scene7.com/is/image/Target/52340452"
                  },
                  {
                    id: "53017049",
                    title:
                      "Women's Dolphin Hem Shorts - Mossimo Supply Co.&#153; Olive XS",
                    class: "BOTTOMS",
                    category: "fashion shorts",
                    redirect_url:
                      "https://www.target.com/p/women-s-dolphin-hem-shorts-mossimo-supply-co-153-olive-xs/-/A-53017049",
                    upc: "490170406281",
                    __order: 7,
                    image_url:
                      "https://target.scene7.com/is/image/Target/53017049"
                  }
                ],
                IDs: [
                  "53193348N0",
                  "52821290N0",
                  "53017106N0",
                  "53017039N0",
                  "52473829N0",
                  "52473803N0",
                  "52340452N0",
                  "53017049N0"
                ]
              },
              meta: {
                vendor: "SAMPLE",
                category: "bottom",
                version: {
                  bottom: {
                    retinanet: "1",
                    embedding: "1"
                  }
                },
                ann_algorithm: "annoy"
              }
            }
          }
        },
        meta: {
          features: {
            source: "VERA",
            category: ["top", "bottom"],
            version: {
              top: {
                retinanet: "1",
                embedding: "4",
                deeplab: "1"
              },
              bottom: {
                retinanet: "1",
                embedding: "1"
              }
            },
            output_type: "document",
            verbose: false
          }
        }
      },
      "6": {
        similar_data: {
          bottom: {
            "0": {
              similar_items: {
                data: [
                  {
                    id: "53017106",
                    title:
                      "Women's Floral Ruffle Shorts - Mossimo Supply Co.&#153; Blue XL",
                    class: "BOTTOMS",
                    category: "fashion shorts",
                    redirect_url:
                      "https://www.target.com/p/women-s-floral-ruffle-shorts-mossimo-supply-co-153-blue-xl/-/A-53017106",
                    upc: "490170411117",
                    __order: 0,
                    image_url:
                      "https://target.scene7.com/is/image/Target/53017106"
                  },
                  {
                    id: "53017039",
                    title:
                      "Women's Striped Dolphin Hem Shorts - Mossimo Supply Co.&#153; Blue M",
                    class: "BOTTOMS",
                    category: "fashion shorts",
                    redirect_url:
                      "https://www.target.com/p/women-s-striped-dolphin-hem-shorts-mossimo-supply-co-153-blue-m/-/A-53017039",
                    upc: "490170405567",
                    __order: 1,
                    image_url:
                      "https://target.scene7.com/is/image/Target/53017039"
                  },
                  {
                    id: "53193348",
                    title:
                      "Women's Plus Size Striped Chambray Pants - Universal Thread&#153; Blue 20W",
                    class: "BOTTOMS",
                    category: "fashion pants",
                    redirect_url:
                      "https://www.target.com/p/women-s-plus-size-striped-chambray-pants-universal-thread-153-blue-20w/-/A-53193348",
                    upc: "490211324260",
                    __order: 2,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_f709a868-cab0-40dd-9eef-559dbdd3881b"
                  },
                  {
                    id: "53017049",
                    title:
                      "Women's Dolphin Hem Shorts - Mossimo Supply Co.&#153; Olive XS",
                    class: "BOTTOMS",
                    category: "fashion shorts",
                    redirect_url:
                      "https://www.target.com/p/women-s-dolphin-hem-shorts-mossimo-supply-co-153-olive-xs/-/A-53017049",
                    upc: "490170406281",
                    __order: 3,
                    image_url:
                      "https://target.scene7.com/is/image/Target/53017049"
                  },
                  {
                    id: "52473829",
                    title:
                      "Women's Bootcut Curvy Bi-Stretch Twill Pants - A New Day&#153; Khaki 10S",
                    class: "BOTTOMS",
                    category: "Trousers",
                    redirect_url:
                      "https://www.target.com/p/women-s-bootcut-curvy-bi-stretch-twill-pants-a-new-day-153-khaki-10s/-/A-52473829",
                    upc: "490180459253",
                    __order: 4,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_4a1315d1-a078-42e5-93f2-8bfb9cc45665"
                  },
                  {
                    id: "52340452",
                    title:
                      "Women's Plus Size Frilled Hem Trouser - Who What Wear&#153; Black 22W",
                    class: "BOTTOMS",
                    category: "Trousers",
                    redirect_url:
                      "https://www.target.com/p/women-s-plus-size-frilled-hem-trouser-who-what-wear-153-black-22w/-/A-52340452",
                    upc: "490211037627",
                    __order: 5,
                    image_url:
                      "https://target.scene7.com/is/image/Target/52340452"
                  },
                  {
                    id: "52821290",
                    title:
                      "Women's High-Rise Raw Hem Straight Jeans - Universal Thread&#153; Light Wash 8 Long",
                    class: "BOTTOMS",
                    category: "Jeans",
                    redirect_url:
                      "https://www.target.com/p/women-s-high-rise-raw-hem-straight-jeans-universal-thread-153-light-wash-8-long/-/A-52821290",
                    upc: "490141157570",
                    __order: 6,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_259f1a97-62b2-4a3e-9002-ce668254b52b"
                  },
                  {
                    id: "52473803",
                    title:
                      "Women's Bootcut Curvy Bi-Stretch Twill Pants - A New Day&#153; Gray 16S",
                    class: "BOTTOMS",
                    category: "Trousers",
                    redirect_url:
                      "https://www.target.com/p/women-s-bootcut-curvy-bi-stretch-twill-pants-a-new-day-153-gray-16s/-/A-52473803",
                    upc: "490180459130",
                    __order: 7,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_b45d5ebb-f37d-40a0-a4bb-4755bf2b4d0e"
                  }
                ],
                IDs: [
                  "53017106N0",
                  "53017039N0",
                  "53193348N0",
                  "53017049N0",
                  "52473829N0",
                  "52340452N0",
                  "52821290N0",
                  "52473803N0"
                ]
              },
              meta: {
                vendor: "SAMPLE",
                category: "bottom",
                version: {
                  bottom: {
                    retinanet: "1",
                    embedding: "1"
                  }
                },
                ann_algorithm: "annoy"
              }
            }
          }
        },
        meta: {
          features: {
            source: "VERA",
            category: ["top", "bottom"],
            version: {
              top: {
                retinanet: "1",
                embedding: "4",
                deeplab: "1"
              },
              bottom: {
                retinanet: "1",
                embedding: "1"
              }
            },
            output_type: "document",
            verbose: false
          }
        }
      },
      "7": {
        similar_data: {
          bottom: {
            "0": {
              similar_items: {
                data: [
                  {
                    id: "53193348",
                    title:
                      "Women's Plus Size Striped Chambray Pants - Universal Thread&#153; Blue 20W",
                    class: "BOTTOMS",
                    category: "fashion pants",
                    redirect_url:
                      "https://www.target.com/p/women-s-plus-size-striped-chambray-pants-universal-thread-153-blue-20w/-/A-53193348",
                    upc: "490211324260",
                    __order: 0,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_f709a868-cab0-40dd-9eef-559dbdd3881b"
                  },
                  {
                    id: "53017106",
                    title:
                      "Women's Floral Ruffle Shorts - Mossimo Supply Co.&#153; Blue XL",
                    class: "BOTTOMS",
                    category: "fashion shorts",
                    redirect_url:
                      "https://www.target.com/p/women-s-floral-ruffle-shorts-mossimo-supply-co-153-blue-xl/-/A-53017106",
                    upc: "490170411117",
                    __order: 1,
                    image_url:
                      "https://target.scene7.com/is/image/Target/53017106"
                  },
                  {
                    id: "52340452",
                    title:
                      "Women's Plus Size Frilled Hem Trouser - Who What Wear&#153; Black 22W",
                    class: "BOTTOMS",
                    category: "Trousers",
                    redirect_url:
                      "https://www.target.com/p/women-s-plus-size-frilled-hem-trouser-who-what-wear-153-black-22w/-/A-52340452",
                    upc: "490211037627",
                    __order: 2,
                    image_url:
                      "https://target.scene7.com/is/image/Target/52340452"
                  },
                  {
                    id: "53017039",
                    title:
                      "Women's Striped Dolphin Hem Shorts - Mossimo Supply Co.&#153; Blue M",
                    class: "BOTTOMS",
                    category: "fashion shorts",
                    redirect_url:
                      "https://www.target.com/p/women-s-striped-dolphin-hem-shorts-mossimo-supply-co-153-blue-m/-/A-53017039",
                    upc: "490170405567",
                    __order: 3,
                    image_url:
                      "https://target.scene7.com/is/image/Target/53017039"
                  },
                  {
                    id: "52473803",
                    title:
                      "Women's Bootcut Curvy Bi-Stretch Twill Pants - A New Day&#153; Gray 16S",
                    class: "BOTTOMS",
                    category: "Trousers",
                    redirect_url:
                      "https://www.target.com/p/women-s-bootcut-curvy-bi-stretch-twill-pants-a-new-day-153-gray-16s/-/A-52473803",
                    upc: "490180459130",
                    __order: 4,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_b45d5ebb-f37d-40a0-a4bb-4755bf2b4d0e"
                  },
                  {
                    id: "52821290",
                    title:
                      "Women's High-Rise Raw Hem Straight Jeans - Universal Thread&#153; Light Wash 8 Long",
                    class: "BOTTOMS",
                    category: "Jeans",
                    redirect_url:
                      "https://www.target.com/p/women-s-high-rise-raw-hem-straight-jeans-universal-thread-153-light-wash-8-long/-/A-52821290",
                    upc: "490141157570",
                    __order: 5,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_259f1a97-62b2-4a3e-9002-ce668254b52b"
                  },
                  {
                    id: "53017049",
                    title:
                      "Women's Dolphin Hem Shorts - Mossimo Supply Co.&#153; Olive XS",
                    class: "BOTTOMS",
                    category: "fashion shorts",
                    redirect_url:
                      "https://www.target.com/p/women-s-dolphin-hem-shorts-mossimo-supply-co-153-olive-xs/-/A-53017049",
                    upc: "490170406281",
                    __order: 6,
                    image_url:
                      "https://target.scene7.com/is/image/Target/53017049"
                  },
                  {
                    id: "52473829",
                    title:
                      "Women's Bootcut Curvy Bi-Stretch Twill Pants - A New Day&#153; Khaki 10S",
                    class: "BOTTOMS",
                    category: "Trousers",
                    redirect_url:
                      "https://www.target.com/p/women-s-bootcut-curvy-bi-stretch-twill-pants-a-new-day-153-khaki-10s/-/A-52473829",
                    upc: "490180459253",
                    __order: 7,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_4a1315d1-a078-42e5-93f2-8bfb9cc45665"
                  }
                ],
                IDs: [
                  "53193348N0",
                  "53017106N0",
                  "52340452N0",
                  "53017039N0",
                  "52473803N0",
                  "52821290N0",
                  "53017049N0",
                  "52473829N0"
                ]
              },
              meta: {
                vendor: "SAMPLE",
                category: "bottom",
                version: {
                  bottom: {
                    retinanet: "1",
                    embedding: "1"
                  }
                },
                ann_algorithm: "annoy"
              }
            }
          }
        },
        meta: {
          features: {
            source: "VERA",
            category: ["top", "bottom"],
            version: {
              top: {
                retinanet: "1",
                embedding: "4",
                deeplab: "1"
              },
              bottom: {
                retinanet: "1",
                embedding: "1"
              }
            },
            output_type: "document",
            verbose: false
          }
        }
      },
      "8": {
        similar_data: {
          bottom: {
            "0": {
              similar_items: {
                data: [
                  {
                    id: "53017106",
                    title:
                      "Women's Floral Ruffle Shorts - Mossimo Supply Co.&#153; Blue XL",
                    class: "BOTTOMS",
                    category: "fashion shorts",
                    redirect_url:
                      "https://www.target.com/p/women-s-floral-ruffle-shorts-mossimo-supply-co-153-blue-xl/-/A-53017106",
                    upc: "490170411117",
                    __order: 0,
                    image_url:
                      "https://target.scene7.com/is/image/Target/53017106"
                  },
                  {
                    id: "52473829",
                    title:
                      "Women's Bootcut Curvy Bi-Stretch Twill Pants - A New Day&#153; Khaki 10S",
                    class: "BOTTOMS",
                    category: "Trousers",
                    redirect_url:
                      "https://www.target.com/p/women-s-bootcut-curvy-bi-stretch-twill-pants-a-new-day-153-khaki-10s/-/A-52473829",
                    upc: "490180459253",
                    __order: 1,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_4a1315d1-a078-42e5-93f2-8bfb9cc45665"
                  },
                  {
                    id: "52821290",
                    title:
                      "Women's High-Rise Raw Hem Straight Jeans - Universal Thread&#153; Light Wash 8 Long",
                    class: "BOTTOMS",
                    category: "Jeans",
                    redirect_url:
                      "https://www.target.com/p/women-s-high-rise-raw-hem-straight-jeans-universal-thread-153-light-wash-8-long/-/A-52821290",
                    upc: "490141157570",
                    __order: 2,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_259f1a97-62b2-4a3e-9002-ce668254b52b"
                  },
                  {
                    id: "53017039",
                    title:
                      "Women's Striped Dolphin Hem Shorts - Mossimo Supply Co.&#153; Blue M",
                    class: "BOTTOMS",
                    category: "fashion shorts",
                    redirect_url:
                      "https://www.target.com/p/women-s-striped-dolphin-hem-shorts-mossimo-supply-co-153-blue-m/-/A-53017039",
                    upc: "490170405567",
                    __order: 3,
                    image_url:
                      "https://target.scene7.com/is/image/Target/53017039"
                  },
                  {
                    id: "53193348",
                    title:
                      "Women's Plus Size Striped Chambray Pants - Universal Thread&#153; Blue 20W",
                    class: "BOTTOMS",
                    category: "fashion pants",
                    redirect_url:
                      "https://www.target.com/p/women-s-plus-size-striped-chambray-pants-universal-thread-153-blue-20w/-/A-53193348",
                    upc: "490211324260",
                    __order: 4,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_f709a868-cab0-40dd-9eef-559dbdd3881b"
                  },
                  {
                    id: "52340452",
                    title:
                      "Women's Plus Size Frilled Hem Trouser - Who What Wear&#153; Black 22W",
                    class: "BOTTOMS",
                    category: "Trousers",
                    redirect_url:
                      "https://www.target.com/p/women-s-plus-size-frilled-hem-trouser-who-what-wear-153-black-22w/-/A-52340452",
                    upc: "490211037627",
                    __order: 5,
                    image_url:
                      "https://target.scene7.com/is/image/Target/52340452"
                  },
                  {
                    id: "53017049",
                    title:
                      "Women's Dolphin Hem Shorts - Mossimo Supply Co.&#153; Olive XS",
                    class: "BOTTOMS",
                    category: "fashion shorts",
                    redirect_url:
                      "https://www.target.com/p/women-s-dolphin-hem-shorts-mossimo-supply-co-153-olive-xs/-/A-53017049",
                    upc: "490170406281",
                    __order: 6,
                    image_url:
                      "https://target.scene7.com/is/image/Target/53017049"
                  },
                  {
                    id: "52473803",
                    title:
                      "Women's Bootcut Curvy Bi-Stretch Twill Pants - A New Day&#153; Gray 16S",
                    class: "BOTTOMS",
                    category: "Trousers",
                    redirect_url:
                      "https://www.target.com/p/women-s-bootcut-curvy-bi-stretch-twill-pants-a-new-day-153-gray-16s/-/A-52473803",
                    upc: "490180459130",
                    __order: 7,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_b45d5ebb-f37d-40a0-a4bb-4755bf2b4d0e"
                  }
                ],
                IDs: [
                  "53017106N0",
                  "52473829N0",
                  "52821290N0",
                  "53017039N0",
                  "53193348N0",
                  "52340452N0",
                  "53017049N0",
                  "52473803N0"
                ]
              },
              meta: {
                vendor: "SAMPLE",
                category: "bottom",
                version: {
                  bottom: {
                    retinanet: "1",
                    embedding: "1"
                  }
                },
                ann_algorithm: "annoy"
              }
            }
          }
        },
        meta: {
          features: {
            source: "VERA",
            category: ["top", "bottom"],
            version: {
              top: {
                retinanet: "1",
                embedding: "4",
                deeplab: "1"
              },
              bottom: {
                retinanet: "1",
                embedding: "1"
              }
            },
            output_type: "document",
            verbose: false
          }
        }
      },
      "9": {
        similar_data: {
          bottom: {
            "0": {
              similar_items: {
                data: [
                  {
                    id: "52340452",
                    title:
                      "Women's Plus Size Frilled Hem Trouser - Who What Wear&#153; Black 22W",
                    class: "BOTTOMS",
                    category: "Trousers",
                    redirect_url:
                      "https://www.target.com/p/women-s-plus-size-frilled-hem-trouser-who-what-wear-153-black-22w/-/A-52340452",
                    upc: "490211037627",
                    __order: 0,
                    image_url:
                      "https://target.scene7.com/is/image/Target/52340452"
                  },
                  {
                    id: "53017106",
                    title:
                      "Women's Floral Ruffle Shorts - Mossimo Supply Co.&#153; Blue XL",
                    class: "BOTTOMS",
                    category: "fashion shorts",
                    redirect_url:
                      "https://www.target.com/p/women-s-floral-ruffle-shorts-mossimo-supply-co-153-blue-xl/-/A-53017106",
                    upc: "490170411117",
                    __order: 1,
                    image_url:
                      "https://target.scene7.com/is/image/Target/53017106"
                  },
                  {
                    id: "52473803",
                    title:
                      "Women's Bootcut Curvy Bi-Stretch Twill Pants - A New Day&#153; Gray 16S",
                    class: "BOTTOMS",
                    category: "Trousers",
                    redirect_url:
                      "https://www.target.com/p/women-s-bootcut-curvy-bi-stretch-twill-pants-a-new-day-153-gray-16s/-/A-52473803",
                    upc: "490180459130",
                    __order: 2,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_b45d5ebb-f37d-40a0-a4bb-4755bf2b4d0e"
                  },
                  {
                    id: "53017049",
                    title:
                      "Women's Dolphin Hem Shorts - Mossimo Supply Co.&#153; Olive XS",
                    class: "BOTTOMS",
                    category: "fashion shorts",
                    redirect_url:
                      "https://www.target.com/p/women-s-dolphin-hem-shorts-mossimo-supply-co-153-olive-xs/-/A-53017049",
                    upc: "490170406281",
                    __order: 3,
                    image_url:
                      "https://target.scene7.com/is/image/Target/53017049"
                  },
                  {
                    id: "53193348",
                    title:
                      "Women's Plus Size Striped Chambray Pants - Universal Thread&#153; Blue 20W",
                    class: "BOTTOMS",
                    category: "fashion pants",
                    redirect_url:
                      "https://www.target.com/p/women-s-plus-size-striped-chambray-pants-universal-thread-153-blue-20w/-/A-53193348",
                    upc: "490211324260",
                    __order: 4,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_f709a868-cab0-40dd-9eef-559dbdd3881b"
                  },
                  {
                    id: "52473829",
                    title:
                      "Women's Bootcut Curvy Bi-Stretch Twill Pants - A New Day&#153; Khaki 10S",
                    class: "BOTTOMS",
                    category: "Trousers",
                    redirect_url:
                      "https://www.target.com/p/women-s-bootcut-curvy-bi-stretch-twill-pants-a-new-day-153-khaki-10s/-/A-52473829",
                    upc: "490180459253",
                    __order: 5,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_4a1315d1-a078-42e5-93f2-8bfb9cc45665"
                  },
                  {
                    id: "53017039",
                    title:
                      "Women's Striped Dolphin Hem Shorts - Mossimo Supply Co.&#153; Blue M",
                    class: "BOTTOMS",
                    category: "fashion shorts",
                    redirect_url:
                      "https://www.target.com/p/women-s-striped-dolphin-hem-shorts-mossimo-supply-co-153-blue-m/-/A-53017039",
                    upc: "490170405567",
                    __order: 6,
                    image_url:
                      "https://target.scene7.com/is/image/Target/53017039"
                  },
                  {
                    id: "52821290",
                    title:
                      "Women's High-Rise Raw Hem Straight Jeans - Universal Thread&#153; Light Wash 8 Long",
                    class: "BOTTOMS",
                    category: "Jeans",
                    redirect_url:
                      "https://www.target.com/p/women-s-high-rise-raw-hem-straight-jeans-universal-thread-153-light-wash-8-long/-/A-52821290",
                    upc: "490141157570",
                    __order: 7,
                    image_url:
                      "https://target.scene7.com/is/image/Target/GUEST_259f1a97-62b2-4a3e-9002-ce668254b52b"
                  }
                ],
                IDs: [
                  "52340452N0",
                  "53017106N0",
                  "52473803N0",
                  "53017049N0",
                  "53193348N0",
                  "52473829N0",
                  "53017039N0",
                  "52821290N0"
                ]
              },
              meta: {
                vendor: "SAMPLE",
                category: "bottom",
                version: {
                  bottom: {
                    retinanet: "1",
                    embedding: "1"
                  }
                },
                ann_algorithm: "annoy"
              }
            }
          }
        },
        meta: {
          features: {
            source: "VERA",
            category: ["top", "bottom"],
            version: {
              top: {
                retinanet: "1",
                embedding: "4",
                deeplab: "1"
              },
              bottom: {
                retinanet: "1",
                embedding: "1"
              }
            },
            output_type: "document",
            verbose: false
          }
        }
      }
    }
  }
};
