import React, { Component } from "react";
import { Helmet } from "react-helmet";
import Slider from "react-slick";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import Loader from "react-loader-spinner";
import "../common/index.scss";
import { connect } from "react-redux";
import axios from "axios";
// import custom Components
import Service from "./common/service";
import BrandBlock from "./common/brand-block";
import NewProduct from "../common/new-product";
import Breadcrumb from "../common/breadcrumb";
import DetailsWithPrice from "./common/product/details-price";
import DetailsTopTabs from "./common/details-top-tabs";
import { addToCart, addToCartUnsafe, addToWishlist } from "../../actions";
import ImageZoom from "./common/product/image-zoom";
import SmallImages from "./common/product/small-image";
import { recommendation } from "../../services/recommendations.js";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import _ from "lodash";

function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{
        ...style,
        display: "block",
        background: "#cfd2d4",
        borderRadius: "16px",
        fontSize: "41px"
      }}
      onClick={onClick}
    />
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{
        ...style,
        display: "block",
        background: "#cfd2d4",
        borderRadius: "16px"
      }}
      onClick={onClick}
    />
  );
}

var settings = {
  dots: false,
  infinite: true,
  slidesToShow: 2,
  speed: 500,
  slidesPerRow: 2,
  slidesToScroll: 2,
  nextArrow: <SampleNextArrow />,
  prevArrow: <SamplePrevArrow />
};

class LeftSideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      nav1: null,
      nav2: null,
      item: null,
      productId: this.props.match.params,
      recommendation: null,
      loader: true
    };
    this.getProductDetail();
  }

  getProductRecommendation = () => {
    let item = this.state.item.data.result;
    let body = { id: item._id, image_url: item.image_url };
    axios.post("/recommendation", body).then(res => {
      if (res.data.error) {
        this.setState({ recommendation: recommendation });
      } else {
        this.setState({ recommendation: res.data.data });
      }
    });
  };

  getProductDetail = () => {
    let sendPromise = axios.get("/product", {
      params: { id: this.state.productId.id }
    });
    sendPromise.then(itemDetail => {
      this.setState({ item: itemDetail }, () => {
        this.getProductRecommendation();
      });
    });
  };

  // document.getElementById('idOfElement').classList.add('newClassName');

  componentDidMount() {
    this.setState({
      nav1: this.slider1,
      nav2: this.slider2
    });
  }

  filterClick() {
    document.getElementById("filter").style.left = "-15px";
  }
  backClick() {
    document.getElementById("filter").style.left = "-365px";
  }

  showLoader = () => {
    return (
      <div style={{ display: "flex", justifyContent: "center" }}>
        <Loader
          type="Puff"
          color="red"
          height={100}
          width={100}
          // timeout={3000} //3 secs
        />
      </div>
    );
  };

  render() {
    const {
      symbol,

      addToCart,
      addToCartUnsafe,
      addToWishlist
    } = this.props;
    let item;
    if (this.state.item != null) {
      item = this.state.item.data.result;
    }
    var products = {
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: false,
      arrows: true,
      fade: true
    };
    var productsnav = {
      slidesToShow: 3,
      swipeToSlide: true,
      arrows: false,
      dots: false,
      focusOnSelect: true
    };
    return (
      <div>
        {item ? (
          <div>
            <Helmet>
              <title>MultiKart | {item.category}</title>
              <meta
                name="description"
                content="Multikart – Multipurpose eCommerce React Template is a multi-use React template. It is designed to go well with multi-purpose websites. Multikart Bootstrap 4 Template will help you run multiple businesses."
              />
            </Helmet>
            <Breadcrumb parent={"Product"} title={item.title} />
            <section className="section-b-space">
              <div className="collection-wrapper">
                <div className="container">
                  <div className="row">
                    <div className="col-sm-3 collection-filter" id="filter">
                      <div className="collection-mobile-back pl-5">
                        <span onClick={this.backClick} className="filter-back">
                          <i
                            className="fa fa-angle-left"
                            aria-hidden="true"
                          ></i>{" "}
                          back
                        </span>
                      </div>

                      {/* <BrandBlock/> */}
                      <Service />
                      {/*side-bar single product slider start*/}
                      <NewProduct item={item} />
                      {/*side-bar single product slider end*/}
                    </div>
                    <div className="col-lg-9 col-sm-12 col-xs-12">
                      <div className="">
                        <div className="row">
                          <div className="col-xl-12">
                            <div className="filter-main-btn mb-2">
                              <span
                                onClick={this.filterClick}
                                className="filter-btn"
                              >
                                <i
                                  className="fa fa-filter"
                                  aria-hidden="true"
                                ></i>{" "}
                                filter
                              </span>
                            </div>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-lg-6 product-thumbnail">
                            <div
                              style={{
                                display: "flex",
                                justifyContent: "center"
                              }}
                            >
                              <ImageZoom image={item.image_url} />
                            </div>

                            <SmallImages
                              item={item}
                              settings={productsnav}
                              avOne={this.state.nav1}
                            />
                          </div>
                          <DetailsWithPrice
                            symbol={symbol}
                            item={item}
                            navOne={this.state.nav1}
                            addToCartClicked={addToCart}
                            BuynowClicked={addToCartUnsafe}
                            addToWishlistClicked={addToWishlist}
                          />
                        </div>
                      </div>
                      <DetailsTopTabs item={item} />
                      <div style={{ padding: "12px", fontSize: "24px" }}>
                        Recommendations:-
                      </div>
                      {this.state.recommendation ? (
                        <Slider {...settings}>
                          {_.map(
                            this.state.recommendation.social_similar_data.top
                              .similar_data.top[0].similar_items.data,
                            (item, index) => {
                              return Array.isArray(
                                this.state.recommendation.social_recommendations
                                  .top[index].similar_data.bottom["0"]
                                  .similar_items.data
                              ) ? (
                                item.image_url ? (
                                  <div
                                    style={{
                                      display: "flex",
                                      padding: "8px",
                                      justifyContent: "center",
                                      flexDirection: "column"
                                    }}
                                  >
                                    <div
                                      style={{
                                        display: "flex",
                                        flexDirection: "row"
                                      }}
                                    >
                                      <img
                                        style={{
                                          width: "52%",
                                          borderRadius: "10px",
                                          boxShadow: "3px 4px 16px #b5aeae",
                                          maxHeight: "26em"
                                        }}
                                        src={item.image_url}
                                      ></img>
                                      <div style={{ paddingLeft: "5px" }}>
                                        <div
                                          style={{
                                            display: "flex",
                                            flexDirection: "row"
                                          }}
                                        >
                                          <div>
                                            <img
                                              style={{
                                                width: "100%",
                                                padding: "7px",
                                                maxWidth: "6em"
                                              }}
                                              src={
                                                this.state.recommendation
                                                  .social_recommendations.top[
                                                  index
                                                ].similar_data.bottom["0"]
                                                  .similar_items.data[1]
                                                  ? this.state.recommendation
                                                      .social_recommendations
                                                      .top[index].similar_data
                                                      .bottom["0"].similar_items
                                                      .data[1].image_url
                                                  : ""
                                              }
                                            ></img>

                                            <div
                                              style={{
                                                width: "100%",
                                                padding: "7px"
                                              }}
                                            >
                                              {this.state.recommendation
                                                .social_recommendations.top[
                                                index
                                              ].similar_data.bottom["0"]
                                                .similar_items.data[1]
                                                ? this.state.recommendation.social_recommendations.top[
                                                    index
                                                  ].similar_data.bottom[
                                                    "0"
                                                  ].similar_items.data[1].title.substring(
                                                    0,
                                                    10
                                                  ) + "..."
                                                : ""}
                                            </div>
                                          </div>
                                          <div>
                                            <img
                                              style={{
                                                width: "100%",
                                                padding: "5px",
                                                maxWidth: "6em"
                                              }}
                                              src={
                                                this.state.recommendation
                                                  .social_recommendations.top[
                                                  index
                                                ].similar_data.bottom["0"]
                                                  .similar_items.data[2]
                                                  ? this.state.recommendation
                                                      .social_recommendations
                                                      .top[index].similar_data
                                                      .bottom["0"].similar_items
                                                      .data[2].image_url
                                                  : ""
                                              }
                                            ></img>
                                            <div
                                              style={{
                                                width: "100%",
                                                padding: "7px"
                                              }}
                                            >
                                              {this.state.recommendation
                                                .social_recommendations.top[
                                                index
                                              ].similar_data.bottom["0"]
                                                .similar_items.data[2]
                                                ? this.state.recommendation.social_recommendations.top[
                                                    index
                                                  ].similar_data.bottom[
                                                    "0"
                                                  ].similar_items.data[1].title.substring(
                                                    0,
                                                    10
                                                  ) + "..."
                                                : ""}
                                            </div>
                                          </div>
                                        </div>
                                        <div
                                          style={{
                                            display: "flex",
                                            flexDirection: "row"
                                          }}
                                        >
                                          <div>
                                            <img
                                              style={{
                                                width: "100%",
                                                padding: "5px",
                                                maxWidth: "6em"
                                              }}
                                              src={
                                                this.state.recommendation
                                                  .social_recommendations.top[
                                                  index
                                                ].similar_data.bottom["0"]
                                                  .similar_items.data[3]
                                                  ? this.state.recommendation
                                                      .social_recommendations
                                                      .top[index].similar_data
                                                      .bottom["0"].similar_items
                                                      .data[3].image_url
                                                  : ""
                                              }
                                            ></img>
                                            <div
                                              style={{
                                                width: "100%",
                                                padding: "7px"
                                              }}
                                            >
                                              {this.state.recommendation
                                                .social_recommendations.top[
                                                index
                                              ].similar_data.bottom["0"]
                                                .similar_items.data[3]
                                                ? this.state.recommendation.social_recommendations.top[
                                                    index
                                                  ].similar_data.bottom[
                                                    "0"
                                                  ].similar_items.data[1].title.substring(
                                                    0,
                                                    10
                                                  ) + "..."
                                                : ""}
                                            </div>
                                          </div>
                                          <div>
                                            <img
                                              style={{
                                                width: "100%",
                                                padding: "5px",
                                                maxWidth: "6em"
                                              }}
                                              src={
                                                this.state.recommendation
                                                  .social_recommendations.top[
                                                  index
                                                ].similar_data.bottom["0"]
                                                  .similar_items.data[5]
                                                  ? this.state.recommendation
                                                      .social_recommendations
                                                      .top[index].similar_data
                                                      .bottom["0"].similar_items
                                                      .data[5].image_url
                                                  : ""
                                              }
                                            ></img>
                                            <div
                                              style={{
                                                width: "100%",
                                                padding: "7px"
                                              }}
                                            >
                                              {this.state.recommendation
                                                .social_recommendations.top[
                                                index
                                              ].similar_data.bottom["0"]
                                                .similar_items.data[2]
                                                ? this.state.recommendation.social_recommendations.top[
                                                    index
                                                  ].similar_data.bottom[
                                                    "0"
                                                  ].similar_items.data[1].title.substring(
                                                    0,
                                                    10
                                                  ) + "..."
                                                : ""}
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <span
                                      style={{
                                        textAlign: "center",
                                        paddingTop: "8px"
                                      }}
                                    >
                                      {this.state.recommendation
                                        .social_recommendations.top[index]
                                        .similar_data.bottom["0"].similar_items
                                        .data[0]
                                        ? this.state.recommendation.social_recommendations.top[
                                            index
                                          ].similar_data.bottom[
                                            "0"
                                          ].similar_items.data[0].title.split(
                                            "&#"
                                          )[0]
                                        : ""}
                                    </span>
                                    <span></span>
                                  </div>
                                ) : (
                                  ""
                                )
                              ) : (
                                ""
                              );
                            }
                          )}
                        </Slider>
                      ) : (
                        this.showLoader()
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        ) : (
          ""
        )}
        {/*Section End*/}
      </div>
    );
  }
}

// const mapStateToProps = (state, ownProps) => {
//   return {
//     ownProps: ownProps
//   };
// };

export default LeftSideBar;
