import React, { Component } from "react";
import Slider from "react-slick";
import { connect } from "react-redux";
import axios from "axios";

import { getTrendingCollection } from "../../../services/index";
import { Product4, Product5 } from "../../../services/script";
import { addToCart, addToWishlist, addToCompare } from "../../../actions/index";
import ProductItem from "../common/product-item";

class TopCollection extends Component {
  constructor(props) {
    super(props);
    this.loadData();
    this.state = {
      products: []
    };
  }

  loadData = () => {
    let sendPromise = axios.get("/products", { params: {} });
    sendPromise.then(result => {
      this.setState({ products: result.data.products });
    });
  };

  render() {
    const { symbol, addToCart, addToWishlist, addToCompare, type } = this.props;

    var properties;
    if (type === "kids") {
      properties = Product5;
    } else {
      properties = Product4;
    }

    return (
      <div>
        {/*Paragraph*/}
        <div className="title1  section-t-space">
          <h4>special offer</h4>
          <h2 className="title-inner1">top collection</h2>
        </div>
        {/*Paragraph End*/}
        <section className="section-b-space p-t-0">
          <div className="container">
            <div className="row">
              <div className="col">
                <Slider
                  {...properties}
                  className="product-4 product-m no-arrow"
                >
                  {this.state.products.length > 0 &&
                    this.state.products.map((product, index) => (
                      <div key={index}>
                        <ProductItem
                          product={product}
                          type="top collection"
                          symbol={symbol}
                          onAddToCompareClicked={() => addToCompare(product)}
                          onAddToWishlistClicked={() => addToWishlist(product)}
                          onAddToCartClicked={() => addToCart(product, 1)}
                          key={index}
                        />
                      </div>
                    ))}
                </Slider>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  items: getTrendingCollection(state.data.products, ownProps.type),
  symbol: state.data.symbol
});

export default connect(mapStateToProps, {
  addToCart,
  addToWishlist,
  addToCompare
})(TopCollection);
