import React, { Component } from "react";
import Slider from "react-slick";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import axios from "axios";
import Loader from "react-loader-spinner";
import { getBestSeller } from "../../services";

let a = [
  {
    id: "52934563",
    title:
      "Womenssss&#39;s Plus Size Star Wars: The Last Jedi Criss Cross V-Neck Short Sleeve Graphic T-Shirt (Juniors&#39;) - Black 2X",
    class: "TOPS",
    category: "Tee shirts",
    redirect_url:
      "https://www.target.com/p/women-39-s-plus-size-star-wars-the-last-jedi-criss-cross-v-neck-short-sleeve-graphic-t-shirt-juniors-39-black-2x/-/A-52934563",
    upc: "490060605367",
    __order: 0,
    image_url:
      "https://target.scene7.com/is/image/Target/GUEST_9914d966-60fe-401a-a60c-8a3a6da16727"
  },
  {
    id: "51980436",
    title: "Women's Embroidered Tie Neck Top Navy S - Cliche",
    class: "TOPS",
    category: "Blouses",
    redirect_url:
      "https://www.target.com/p/women-s-embroidered-tie-neck-top-navy-s-cliche/-/A-51980436",
    upc: "849871032372",
    __order: 1,
    image_url: "https://target.scene7.com/is/image/Target/51980436"
  },
  {
    id: "17136836",
    title:
      "Women's Plus Size Cardigan Black Print 2X - Mossimo Supply Co.&#153;",
    class: "TOPS",
    category: "Cardigans",
    redirect_url:
      "https://www.target.com/p/women-s-plus-size-cardigan-black-print-2x-mossimo-supply-co-153/-/A-17136836",
    upc: "490210830182",
    __order: 2,
    image_url: "https://target.scene7.com/is/image/Target/17136836"
  },
  {
    id: "52300578",
    title:
      "Women's Plus Size Striped V-Neck T-Shirt - Ava & Viv&#153; Black 3X",
    class: "TOPS",
    category: "Tee shirts",
    redirect_url:
      "https://www.target.com/p/women-s-plus-size-striped-v-neck-t-shirt-ava-viv-153-black-3x/-/A-52300578",
    upc: "490210152970",
    __order: 3,
    image_url:
      "https://target.scene7.com/is/image/Target/GUEST_b2dea12c-4191-400c-be7c-af6925dd2fc4"
  },
  {
    id: "52300576",
    title: "Women's Plus Size V-Neck T-Shirt - Ava & Viv&#153; Opulent Red 1X",
    class: "TOPS",
    category: "Tee shirts",
    redirect_url:
      "https://www.target.com/p/women-s-plus-size-v-neck-t-shirt-ava-viv-153-opulent-red-1x/-/A-52300576",
    upc: "490210153113",
    __order: 4,
    image_url: "https://target.scene7.com/is/image/Target/52300576"
  },
  {
    id: "51114351",
    title: "Women's Woven Popover Zipper Neck Black S - Zac and Rachel",
    class: "TOPS",
    category: "Blouses",
    redirect_url:
      "https://www.target.com/p/women-s-woven-popover-zipper-neck-black-s-zac-and-rachel/-/A-51114351",
    upc: "884148534609",
    __order: 5,
    image_url: "https://target.scene7.com/is/image/Target/51114351"
  },
  {
    id: "53183269",
    title:
      "Women's Plus Size Printed V-Neck Short Sleeve T-Shirt - Ava & Viv&#153; Cream 3X",
    class: "TOPS",
    category: "Tee shirts",
    redirect_url:
      "https://www.target.com/p/women-s-plus-size-printed-v-neck-short-sleeve-t-shirt-ava-viv-153-cream-3x/-/A-53183269",
    upc: "490210155933",
    __order: 6,
    image_url: "https://target.scene7.com/is/image/Target/53183269"
  },
  {
    id: "75559140",
    title:
      "Women's Short Sleeve Woven T-Shirt with Hardware - Lux II - Purple M",
    class: "TOPS",
    category: "Blouses",
    redirect_url:
      "https://www.target.com/p/women-s-short-sleeve-woven-t-shirt-with-hardware-lux-ii-purple-m/-/A-75559140",
    upc: "191906004916",
    __order: 7,
    image_url:
      "https://target.scene7.com/is/image/Target/GUEST_7fb408ae-3b11-4a70-8298-1de1affece86"
  }
];
class NewProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: this.props.item,
      itemsArr: []
    };
    this.loadSimilarData();
  }

  loadSimilarData() {
    var data = {
      category: this.state.item.category,
      image_url: this.state.item.image_url
    };
    axios
      .post("imagesearch", {
        params: {
          source: "SAMPLE",
          category: this.state.item.category,
          nitems: 8,
          vendor: "Walmart"
        },

        data: data
      })
      .then(response => {
        if (
          !response.data.error &&
          Array.isArray(response.data.data) &&
          response.data.data.length
        )
          this.setState({ itemsArr: [...response.data.data] });
        else this.setState({ itemsArr: a });
      });
  }

  renderSlider = arrays => {
    return (
      <Slider className="offer-slider slide-1">
        {arrays.map((products, index) => (
          <div key={index}>
            {products.map((product, i) => (
              <div className="media" key={i}>
                <Link
                  to={`${process.env.PUBLIC_URL}/left-sidebar/product/${product.id}`}
                >
                  <img className="img-fluid" src={product.image_url} alt="" />
                </Link>
                <div className="media-body align-self-center">
                  <div className="rating">
                    <i className="fa fa-star"></i>
                    <i className="fa fa-star"></i>
                    <i className="fa fa-star"></i>
                    <i className="fa fa-star"></i>
                    <i className="fa fa-star"></i>
                  </div>
                  <Link
                    to={`${process.env.PUBLIC_URL}/left-sidebar/product/${product.id}`}
                  >
                    <h6>{product.title}</h6>
                  </Link>
                  {/* <h4>
                {symbol}
                {(product.price * product.discount) / 100}
                <del>
                  <span className="money">
                    {symbol}
                    {product.price}
                  </span>
                </del>
              </h4> */}
                </div>
              </div>
            ))}
          </div>
        ))}
      </Slider>
    );
  };

  renderLoader = () => {
    return (
      <div style={{ display: "flex", justifyContent: "center" }}>
        <Loader
          type="Puff"
          color="red"
          height={100}
          width={100}
          // timeout={3000} //3 secs
        />
      </div>
    );
  };

  render() {
    const { itemsArr, symbol } = this.state;
    const items = [...itemsArr];
    var arrays = [];
    while (items.length > 0) {
      arrays.push(items.splice(0, 3));
    }

    return (
      <div className="theme-card">
        <h5 className="title-border">similar products</h5>
        {arrays.length ? this.renderSlider(arrays) : this.renderLoader()}
      </div>
    );
  }
}

// function mapStateToProps(state) {
//   return {
//     items: getBestSeller(state.data.products),
//     symbol: state.data.symbol
//   };
// }

export default NewProduct;
