FROM ubuntu:latest
RUN apt-get update && \
  apt-get install -y software-properties-common
RUN apt-get update
RUN apt-get install -y build-essential  python3.6 python3-venv  apt-utils libjpeg8-dev zlib1g-dev
RUN apt-get install -y git
RUN apt install -y python3-pip
RUN python3 -m pip install wheel
RUN apt-get install -y gcc

MAINTAINER gaurav@piktorlabs

ENV PYTHONUNBUFFERED 1

COPY ./requirements.txt  /requirements.txt
RUN pip3 install -r requirements.txt
RUN mkdir /app
WORKDIR /app
COPY . /app
