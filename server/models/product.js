const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const metaSchema = new Schema({
  alternate_image: {
    type: Array
  },
  brand: {
    type: String
  },
  brand_brand: {
    type: String
  },
  img_url: {
    type: String
  },
  name: {
    type: String
  },
  orig_alt_images: {
    type: Array
  },
  price: {
    type: String
  },
  sizes: {
    type: Array
  },
  url: {
    type: String
  }
});

const ProductSchema = new Schema({
  brand: { type: String },
  color: { type: String },
  tcin: { type: String },
  meta: { type: metaSchema },
  sizes: { type: Array },
  title: { type: String },
  url: { type: String },
  from: { type: String },
  category: { type: String },
  type: { type: String }
});

module.exports = mongoose.model("products", ProductSchema, "products");
