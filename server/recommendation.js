const fs = require("fs");
const tf = require("@tensorflow/tfjs");
const DeltaE = require("delta-e");
const color_threshold = 20;
const num_results = 10;

console.log("loading embeddings");

const wallTops = JSON.parse(fs.readFileSync("./server/embeddings.json"));

console.log("embeddings loaded\n");
console.log("recommending engine is up and running\n");

let embedding_arr = [];
let id_arr = [];


wallTops.data.map(data => {
  embedding_arr.push(data.embeddings);
  id_arr.push(data.tcin);
});

const euclidean = function(a, b, k = 10) {
  const dists = tf.norm(tf.sub(a, b), 1, (axis = 1));
  const { _, indices } = tf.topk(tf.mul(-1, dists), k);
  return indices.dataSync();
};

const top_to_wallmart = tcin => {
  const index = wallTops.data.find(e => {
    if (String(tcin) == String(e.tcin)) {
      return e;
    }
  });

  try {
    let tops_social = euclidean(
      index.embeddings,
      embedding_arr,
      (k = num_results)
    );
    const resultedArr = [];
    tops_social.map(i => {
      resultedArr.push(id_arr[i]);
    });
    return resultedArr;
  } catch (e) {
    return { error: e };
  }
};


module.exports = { top_to_wallmart };
